# Welcome to *photoncruncher* #
*photoncruncher* is by no means a finished program. It has been started from scratch and is still growing and improving. Some parts have completely changed since the begining, others have remained unchanged. Some functions were used and tested in the beginning, others have only recently been added and tested. All results should be scrutinized for correctness. 

# Installation #

## Prerequisites ##
photoncruncher relies on a number of open-source projects that have to be installed beforehand in order for it to work properly. This includes

* python (version 2.7.X)
* vtk (version 5.10.X)
* hdf5 (version 1.6.X)
* eigen3 (version 3.1.4)

A good way to install these packages on Mac OS X is to use MacPorts (www.macports.org). A detailed description on how to install MacPorts itself can be found on their homepage.

### Installing python2.7 ###
Currently, everything is only tested with python2.7. Other versions may work but are not tested and not supported. Please drop me a note if you find that it works flawlessly with another version of python. In order to be able to use the various packages for python from macports, it's also necessary to install the macports python version (This will not affect the version installed by Mac OS X and you can at anytime switch back with `$ port select python`). 

```
$ sudo port install py27 py27-pyqt4 [py27-numpy]
```
Numpy can either be installed with macports or with pip. If you are using virtualenv (which is recommended), installing numpy with pip is recommended:
```
$ sudo port install py27-pip
$ pip-2.7 install numpy
```
Note: Installing pyqt4 with pip is a hassle, it's easier to install pyqt4 with macports (as described above) and create links from your virtualenv to the macports version. The bash script photoncruncher-virtualenv-setup.sh might give you hint's how to do that on your system.

### Installing vtk ###
*photoncruncher* requires at least `vtk-5.10` with python wrapping and qt4 support. By chosing the right port variant (read about port variants [here](http://guide.macports.org/#using.variants) )
```
$ sudo port install vtk5 +python27 +qt4_mac
```
Note that the installation of these packages may take a while, since the qt4 dependency has to be resolved (resulting in the build of the qt4 package). 



### Installing eigen3 ###
```
$ sudo port install eigen3
```



### Installing photoncruncher ###
If you followed the previous steps carefully, you should be able to install the *photoncruncher* package with

```
$ pip install git+https://bitbucket.org/boecklic/python-photoncruncher
```

or if you want to upgrade your existing installation 

```
$ pip install -U git+https://bitbucket.org/boecklic/python-photoncruncher
```


# Usage #

The package contains two python scripts, MonteCarloSimulator.py and photoncruncher-gui.py. The gui helps creating configuration files that are used as input for the MonteCarloSimulator.py. The gui uses a few shortcuts

- **r**: Reprint the graphic section
- **o**: Load/Open an existing configuration file
- **s**: Save the configuration
- **w**: Quit
- **a**: Toggle the display of the axes
- **c**: Edit options for displaying the graphics
- **p**: Save the graphics window to a .png file

A sample configuration file might look like this:
```

DebugLevel = INFO
ResultDir = <ABSOLUTE PATH TO YOUR RESULT DIRECTORY>/test_from_pip
MaterialDB = <ABSOLUTE PATH TO YOUR MATERIAL DATABASE>/materials.db
[RNG]
	GslRngType = mt19937
	GslRngSeed = 0

[MCSimulator]
	NumberOfPhotons = 100
	MonteCarloMethod = pcPhotonPacketVariableStepsizeStepping2
	[[MCMethod]]
		[[[pcPhotonPacketVariableStepsizeStepping2]]]
			SetWeightThreshold = 0.1
			SetRouletteChance = 10.0


	[[Geometry]]
		type = pcHomogeneousRegularGridGeometry
		[[[pcHomogeneousRegularGridGeometry]]]
			SetOrigin = 0.0,0.0,0.0
			SetSpacing = 1.0,1.0,1.0
			SetDimensions = 11,11,11
			mu_a = 1.0
			mu_s = 10.0
			g = 0.9
			n = 1.5

		[[[pcRegularGridGeometry]]]
			SetVtkImageDataObject = 


	[[Sources]]
		type = pcIsotropicPhotonPointSource
		[[[pcPhotonPointSource]]]
			SetOrigin = 5.0,5.0,5.0
			SetOutputDirection = 1.0,0.0,0.0
			SetNumericalAperture = 0.0
			SetInitialPhotonWeight = 100.0

		[[[pcPhotonIsotropicPointSource]]]
			SetOrigin = 5.0,5.0,5.0
			SetInitialPhotonWeight = 100.0



[Observers]
	[[ObserverCircles]]

	[[ObserverRectangles]]
		[[[pcObserverRectangle%0]]]
			SetDescription = rectangle1
			SetSideB = 1.0
			SetSideA = 2.0
			SetOutputFileName = rectangle1
			SetNormal = 0.0,1.0,0.0
			SetRectangleNorth = 0.0,0.0,1.0
			SetOrigin = 5.0,10.0,5.0
			SaveIntermediateSteps = true
			SetMode = detector


	[[StructuredObservers]]


```

The material database (material.db) contains entries of the following form:
```
# the units of the coefficients are
# mm, (mm^-1)


[Vacuum]
ID = 0
name = "Vacuum"
mu_a = 1.0
mu_s = 0.1
g = 0.98
n = 1.0

[GreyMatter]
ID = 4
name = "Grey Matter"
mu_a = 0.27
mu_s = 37.0
g = 0.94
n = 1.3
```

The simulation software is eventually started with
```
$ MonteCarloSimulator.py config.cfg
```

## Results ##

Volume data is stored as .vti file an can be analyzed with [paraview](http://www.paraview.org/). All other results are stored in one file in [HDF5](http://www.hdfgroup.org/HDF5/) format. This file format can be loaded by [Matlab](http://www.mathworks.ch/ch/help/matlab/ref/h5read.html), [python](https://github.com/h5py/h5py) or the [HDF5 viewer](http://www.hdfgroup.org/hdf-java-html/hdfview/). With the [hdf5tools](http://www.hdfgroup.org/products/hdf5_tools/) you can even create text-dumps.