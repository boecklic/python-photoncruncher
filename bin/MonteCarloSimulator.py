#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import shutil
from configobj import ConfigObj
from validate import Validator

# import photoncruncher as pc

from pctools.mcs import setter,in_result_directory,ReadVtkImageFromFile,MonteCarloSimulator

# from distutils.sysconfig import get_python_lib
from pkg_resources import resource_filename

def printUsage():
	print "Welcome to MonteCarloSimulator!";
	print "=====================================\n";
	print "Usage: MonteCarloSimulator.py config.cfg\n";


		# special case for the homogeneous block, as the material is provided by the Batch-Script
		# ----------------------------------------------------
		# ==> This has to go to derived class!!!!!!!1
		# if geotype == 'pcHomogeneousBlockGeometry' or geotype == 'pcHomogeneousRegularGridGeometry':
		# 	try:
		# 	  	homomat = options['homogeneousMaterial'];
		# 		pc.PLOG(pc.INFO,'Using a block of homogeneous material:' + str(homomat));
		# 		geoObject.SetMaterial(homomat);
		# 	except Exception as e:
		# 		pc.PLOG(pc.ERR,'homogeneousMaterial not defined in options')
		# 		pc.PLOG(pc.ERR,str(e))
		# 		sys.exit(1);
		# ----------------------------------------------------









def main(conf):

	
	# try:
	# 	conf = options['conf'];
	# 	# print conf
	# except:
	# 	pc.PLOG(pc.ERR,'No config object found!')
	# 	sys.exit(1);



	mcs = MonteCarloSimulator(conf)
	# mcs.callList['EnableConsoleLogging'] = False

	mcs.Exec()




#===========================================================#
# If this script is called as main
#===========================================================#
if __name__ == '__main__':
	if len(sys.argv) < 2 or len(sys.argv) > 3 or '--help' in sys.argv:
		printUsage()
	else:
		# args = sys.argv;

		# options = {};
		

		
		# if '--visualize' in argv:
		# 	pc.PLOG(pc.INFO,'we only parse the config and visualize the different components')
		# 	options['visualize'] = True
		# else:
		# 	options['visualize'] = False
		
		
		

		# conf = ConfigObj(args[1],configspec=confspecfile,indent_type='\t')
		# val = Validator()
		# result = conf.validate(val, preserve_errors=False, copy=True)	

		# options['conf'] = conf;
		# options['batch'] = False;
		main(sys.argv[1])

