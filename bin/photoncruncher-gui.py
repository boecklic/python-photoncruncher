#!/usr/bin/env python      

#==================================================
#
# COPYRIGHT: (c) Christoph Boecklin
#                boecklic@ifh.ee.ethz.ch
#
#==================================================
#
#           Version: 0.1
#           Date:    18.7.2013
#
#==================================================


import cStringIO
import sys
import os
from configobj import ConfigObj
from validate import Validator
import vtk
import numpy as np

from PyQt4 import QtGui
from PyQt4 import QtCore
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

# from distutils.sysconfig import get_python_lib
from pkg_resources import resource_filename

# Load the UIs generated with the QtDesigner
# from photoncruncher_ui import Ui_MainWindow
# from ObserverPlaneCircle_ui import Ui_ObserverPlaneCircleDialog
# from ObserverRectangle_ui import Ui_ObserverRectangleDialog
# from StructuredFluenceObserver_ui import Ui_StructuredFluenceObserverDialog
# from GeometryDisplayOptions_ui import Ui_GeometryDisplayOptions

from pctools.gui import Ui_MainWindow
from pctools.gui import Ui_ObserverPlaneCircleDialog
from pctools.gui import Ui_ObserverRectangleDialog
from pctools.gui import Ui_StructuredFluenceObserverDialog
from pctools.gui import Ui_GeometryDisplayOptions


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s



class Struct(object):
    pass



class pcConfigSection(object):

    def __init__(self,name):
        self.name = name
        self.elems = []

    def __str__(self):
        return self.name

    def add(self,elem):
        self.elems.append(elem)

    def clear(self):
        self.elems[:] = []

    def tostr(self,indent=-1):
        os = cStringIO.StringIO()
        if self.name != 'root':
            print >>os, '{}{}{}{}'.format(indent * '\t',(indent+1) * '[',self.name,(indent+1)*']')
        level = indent
        for elem in self.elems:
            print >>os, elem.tostr(level+1)
        return os.getvalue()

    def todict(self):
        dct = {}
        dct[self.name] = {}
        for elem in self.elems:
            dct[self.name][elem.name] = elem.todict()
        return dct


    def fromstr(self,cfdict):
        for elem in self.elems:
            # print 'LOG: [] section {}'.format(elem.name)
            # print 'LOG: cfdict {}'.format(str(cfdict))
            if elem.name in cfdict:
                # print 'LOG: found elem.name in cfdict'
                elem.fromstr(cfdict[elem.name])
                
        



class pcConfigEntry(object):
    def __init__(self,name,widget):
        self.name = name
        self.widget = widget

    def __str__(self):
        return self.name

    def tostr(self,indent=0):
        try:
            if type(self.widget) == type(''):
                val = self.widget
            elif callable(self.widget.get):
                val = self.widget.get()
            else:
                print 'ERR: can\'t call the widgets get method and it\'s not a string!'
            return "{}{} = {}".format(indent * '\t',self.name,val)
        except Exception as e:
            print 'ERR: something went wrong {} while calling tostr for {}'.format(str(e),self.name)

        

    def todict(self):
        if hasattr(self.widget,'todict'):
            val = self.widget.todict()
        elif type(self.widget) == type(''):
            val = self.widget
        elif callable(self.widget.get):
            val = self.widget.get()
        else:
            print 'ERR: can\'t call the widgets todict or get method and it\'s not a string!'

        return val

    def fromstr(self,value):
        print 'LOG: trying to set {} for {}'.format(value,self.name)
        if type(self.widget) == type(''):
            self.widget = str(value)
        elif callable(self.widget.set):
            self.widget.set(value)
        else:
            print 'ERR: can\'t call the widgets set method and it\'s not a string!'






class QtInteractor(object):
    """
    Base Class for standardized interaction with Qt Elements. Every subclass should point the setter and getter to 
    correct corresponding function name 
    """
    def __init__(self,widget):
 
        self.widget = widget

    def get(self):
        return self.getter()

    def set(self,*args,**kwargs):
        self.setter(*args,**kwargs)

class QtlineEditInteractor(QtInteractor):
    def __init__(self,widget):
        super(QtlineEditInteractor,self).__init__(widget)

        if not hasattr(self.widget,"text"):
            print "ERR: cant find text attribute, doesn't seem to be a lineEdit???"
            sys.exit(1)

        self.setter = self.widget.setText
        self.getter = lambda : str(self.widget.text())


class QtcomboBoxInteractor(QtInteractor):
    def __init__(self,widget,vals):
        super(QtcomboBoxInteractor,self).__init__(widget)

        self.vals = vals
        self.setter = lambda key: self.widget.setCurrentIndex(self.vals[str(key)])
        self.getter = self.widget.currentText


class QtcheckBoxInteractor(QtInteractor):
    def __init__(self,widget,vals):
        """ vals has to be in the form: {'value1':'True','value2':'False'}"""
        super(QtcheckBoxInteractor,self).__init__(widget)

        """ Todo: how to programmatically fire event?"""

        self.vals = vals
        self.invertedvals = {}
        for key,val in self.vals.iteritems():
            self.invertedvals[str(val)] = key
        self.setter = lambda val: self.widget.setChecked(self.vals[val])
        self.getter = lambda : self.invertedvals[str(self.widget.isChecked())]

class QtspinBoxInteractor(QtInteractor):
    def __init__(self,widget,dattype=float):
        super(QtspinBoxInteractor,self).__init__(widget)

        self.setter = lambda x: self.widget.setValue(float(x))
        self.getter = self.widget.value

class QtvectorInteractor(QtInteractor):
    def __init__(self,widget_X,widget_Y,widget_Z,dattype=float):
        super(QtvectorInteractor,self).__init__(None)
        self.wX = widget_X
        self.wY = widget_Y
        self.wZ = widget_Z
        if dattype == float:
            self.converter = lambda x: float(x)
        else:
            self.converter = lambda x: int(x)
        self.setter = self._setter
        self.getter = lambda : '{},{},{}'.format(self.wX.value(),self.wY.value(),self.wZ.value())

    def _setter(self,val):
        
        self.wX.setValue(self.converter(val[0]))
        self.wY.setValue(self.converter(val[1]))
        self.wZ.setValue(self.converter(val[2]))

    def todict(self):
        return [self.wX.value(),self.wY.value(),self.wZ.value()]






class ObserverTreeWidgetItem(QtGui.QTreeWidgetItem):
    counter = -1
    def __init__(self,parent,**kwargs):
        super(ObserverTreeWidgetItem,self).__init__(parent)
        self.parent = parent
        self.counter += 1

        if not 'dialog' in kwargs:
            print 'ERR: I need a dialog to edit my entry'
        else:
            self.dlg = kwargs['dialog']

        if 'name' in kwargs:
            self.name = kwargs['name']
        else:
            self.name = self._getDefaultName()
        self.conf = Struct()
        self.conf.root = pcConfigSection(self.name)

        self.act = None

        self.setText(0,self.name)

    def _getDefaultName(self):
        return "NoNameForAbstractObserverItem"

        
        
    def tostr(self,indent=0):
        os = cStringIO.StringIO()
        if self.name != 'root':
            print >>os, '{}{}{}{}'.format(indent * '\t',(indent+1) * '[',self.name,(indent+1)*']')
        
        print self.dct
        for key,val in self.dct.iteritems():
            if isinstance(val,list):
                print >>os, "{0}{1} = {2[0]},{2[1]},{2[2]}".format((indent+1) * '\t',key,val)    
            else:
                print >>os, "{}{} = {}".format((indent+1) * '\t',key,val)
    
        return os.getvalue()

    def showDialog(self):
        self.dlg.displayItem(self)
        self.dlg.exec_()

    def setValues(self,dct):
        print dct
        self.dct = dct

    def getValues(self):
        return self.dct

    def addToRenderer(self,ren):
        pass

    def removeFromRenderer(self,ren):
        if self.act != None:
            self.act.removeFromRenderer(ren)

    def update(self,**kwargs):
        if self.act != None:
            self.act.update(**kwargs)
            



class ObserverCircleTreeWidgetItem(ObserverTreeWidgetItem):
    
    def __init__(self,parent,**kwargs):
        super(ObserverCircleTreeWidgetItem,self).__init__(parent,**kwargs)

    def _getDefaultName(self):
        name = 'pcObserverCircle%{}'.format(self.counter)
        self.counter += 1
        return name

    def addToRenderer(self,ren):
        tofloat = lambda vec: map(float,vec)
        if float(self.dct['SetRadius']) > 0.0:
            self.act = VtkPlaneCircle(origin=tofloat(self.dct['SetOrigin']),normal=tofloat(self.dct['SetNormal']),radius=float(self.dct['SetRadius']))
        else:
            self.act = VtkPlaneCircle(origin=tofloat(self.dct['SetOrigin']),normal=tofloat(self.dct['SetNormal']))
        self.act.addToRenderer(ren)

    def update(self,**kwargs):
        tofloat = lambda vec: map(float,vec)
        if float(self.dct['SetRadius']) > 0.0:
            super(ObserverCircleTreeWidgetItem,self).update(origin=tofloat(self.dct['SetOrigin']),normal=tofloat(self.dct['SetNormal']),radius=float(self.dct['SetRadius']),**kwargs)
        else:
            super(ObserverCircleTreeWidgetItem,self).update(origin=tofloat(self.dct['SetOrigin']),normal=tofloat(self.dct['SetNormal']),**kwargs)


class ObserverRectangleTreeWidgetItem(ObserverTreeWidgetItem):
    
    def __init__(self,parent,**kwargs):
        super(ObserverRectangleTreeWidgetItem,self).__init__(parent,**kwargs)

    def _getDefaultName(self):
        name = 'pcObserverRectangle%{}'.format(self.counter)
        self.counter += 1
        return name

    def addToRenderer(self,ren):
        tofloat = lambda vec: map(float,vec)
        self.act = VtkRectangle(origin=tofloat(self.dct['SetOrigin']),
                                normal=tofloat(self.dct['SetNormal']),
                                sideA=float(self.dct['SetSideA']),
                                sideB=float(self.dct['SetSideB']),
                                rectangleNorth=tofloat(self.dct['SetRectangleNorth']))
        self.act.addToRenderer(ren)

    def update(self,**kwargs):
        print 'calling the upadte on ObserverRectangleTreeWidgetItem'
        tofloat = lambda vec: map(float,vec)
        super(ObserverRectangleTreeWidgetItem,self).update(origin=tofloat(self.dct['SetOrigin']),
                                normal=tofloat(self.dct['SetNormal']),
                                sideA=float(self.dct['SetSideA']),
                                sideB=float(self.dct['SetSideB']),
                                rectangleNorth=tofloat(self.dct['SetRectangleNorth']),
                                **kwargs)

class StructuredObserverTreeWidgetItem(ObserverTreeWidgetItem):

    def __init__(self,parent,**kwargs):
        super(StructuredObserverTreeWidgetItem,self).__init__(parent,**kwargs)

    def _getDefaultName(self):
        name = 'pcStructuredFluenceObserver%{}'.format(self.counter)
        self.counter += 1
        return name
    # def update(sefl,)

class ObserverTreeWidgetHandler(pcConfigSection):

    def __init__(self,name,qtreewidget,vtkrenderer,repaint_callback,vtkimagefile):
        super(ObserverTreeWidgetHandler,self).__init__(name)
        self.obsw = qtreewidget
        self.ren = vtkrenderer
        self.repaint = repaint_callback
        
        QtCore.QObject.connect(self.obsw, QtCore.SIGNAL(_fromUtf8("itemDoubleClicked(QTreeWidgetItem*,int)")), self.editObserverItem)
        QtCore.QObject.connect(self.obsw, QtCore.SIGNAL("itemSelectionChanged()"),self.selectObserverItem)

        self.topLevelItems = {}

        # self.ObserverCirclesConf = None
        self.ObserverCirclesConf = pcConfigSection('ObserverCircles')
        self.add(self.ObserverCirclesConf) 
        self.ObserverCircleDialog = ObserverCircleDialog()
        self.topLevelItems['ObserverCircles'] = QtGui.QTreeWidgetItem(self.obsw)
        self.topLevelItems['ObserverCircles'].setText(0, "Circles")

        self.ObserverRectanglesConf = pcConfigSection('ObserverRectangles')
        self.add(self.ObserverRectanglesConf)
        self.ObserverRectangleDialog = ObserverRectangleDialog()
        self.topLevelItems['ObserverRectangles'] = QtGui.QTreeWidgetItem(self.obsw)
        self.topLevelItems['ObserverRectangles'].setText(0, "Rectangles")  


        self.StructuredObserversConf = pcConfigSection('StructuredObservers')
        self.add(self.StructuredObserversConf)
        self.StructuredObserverDialog = StructuredFluenceDialog(vtkimagefile=vtkimagefile)
        self.topLevelItems['StructuredObservers'] = QtGui.QTreeWidgetItem(self.obsw)
        self.topLevelItems['StructuredObservers'].setText(0, "Structured Observers")        

    def editObserverItem(self,wdgt,index):
        
        # Only Items that subclass from ObserverTreeWidgetItem have a showDialog method,
        # the toplevel item doesn't
        if hasattr(wdgt,'showDialog'):
            wdgt.showDialog()
    
    def fromstr(self,cfdict):
        
        if 'ObserverCircles' in cfdict:
            
            """remove items from tree"""
            for child in self.topLevelItems['ObserverCircles'].takeChildren():
                self.topLevelItems['ObserverCircles'].removeChild(child)
            
            """remove items from config/renderer"""
            if len(self.ObserverCirclesConf.elems) > 0:
                """remove vtkActor from renderer"""
                for elem in self.ObserverCirclesConf.elems:
                    elem.removeFromRenderer(self.ren)
                # remove from config
                self.ObserverCirclesConf.clear()    
            
            for obskey,obsval in cfdict['ObserverCircles'].iteritems():
                obs = ObserverCircleTreeWidgetItem(self.topLevelItems['ObserverCircles'],dialog=self.ObserverCircleDialog,name=obskey)
                obs.setValues(obsval)
                obs.addToRenderer(self.ren)
                self.ObserverCirclesConf.add(obs)


        if 'ObserverRectangles' in cfdict:
            """remove items from tree"""
            for child in self.topLevelItems['ObserverRectangles'].takeChildren():
                self.topLevelItems['ObserverRectangles'].removeChild(child)

            """remove items from config/renderer"""
            if len(self.ObserverRectanglesConf.elems) > 0:
                """remove vtkActor from renderer"""
                for elem in self.ObserverRectanglesConf.elems:
                    elem.removeFromRenderer(self.ren)
                # remove from config
                self.ObserverRectanglesConf.clear()    
            
            for obskey,obsval in cfdict['ObserverRectangles'].iteritems():
                obs = ObserverRectangleTreeWidgetItem(self.topLevelItems['ObserverRectangles'],dialog=self.ObserverRectangleDialog,name=obskey)
                obs.setValues(obsval)
                obs.addToRenderer(self.ren)
                self.ObserverRectanglesConf.add(obs)

        if 'StructuredObservers' in cfdict:
            """remove items from tree"""
            for child in self.topLevelItems['StructuredObservers'].takeChildren():
                self.topLevelItems['StructuredObservers'].removeChild(child)

            """remove items from config/renderer => nothing to do for the structured observer"""
            # if len(self.StructuredObserversConf.elems) > 0:
            #     """remove vtkActor from renderer"""
            #     for elem in self.StructuredObserversConf.elems:
            #         elem.removeFromRenderer(self.ren)
            #     # remove from config
            #     self.StructuredObserversConf.clear()    
            
            for obskey,obsval in cfdict['StructuredObservers'].iteritems():
                obs = StructuredObserverTreeWidgetItem(self.topLevelItems['StructuredObservers'],dialog=self.StructuredObserverDialog,name=obskey)
                obs.setValues(obsval)
                # obs.addToRenderer(self.ren)
                self.StructuredObserversConf.add(obs)

    def addObserver(self,observerType,**kwargs):

        # dlg = ObserverCircleDialog()
        if observerType == 'Observer Plane/Circle':
            dlg = self.ObserverCircleDialog
            cnf = self.ObserverCirclesConf
            wdgtitem = ObserverCircleTreeWidgetItem
            tli = self.topLevelItems['ObserverCircles']
        elif observerType == 'Observer Rectangle':
            dlg = self.ObserverRectangleDialog
            cnf = self.ObserverRectanglesConf
            wdgtitem = ObserverRectangleTreeWidgetItem
            tli = self.topLevelItems['ObserverRectangles']
        elif observerType == 'Structured Fluence Observer':
            dlg = self.StructuredObserverDialog
            cnf = self.StructuredObserversConf
            wdgtitem = StructuredObserverTreeWidgetItem
            tli = self.topLevelItems['StructuredObservers']

        class Helper(object):
            def __init__(self):
                self.dct = {}
            def getValues(self):
                return self.dct

            def setValues(self,dct):
                self.dct = dct

            def update(self,**kwargs):
                pass

        # Display the correct dialog, setting the values from the dummy helper class
        helper = Helper()
        dlg.displayItem(helper)
        dlg.exec_()

        if helper.getValues() != {}:
            """if helper.getValues() is {} cancelled was pressed and we don't want to add a new Observer"""
            obs = wdgtitem(tli,dialog=dlg)
            obs.setValues(helper.getValues())
            obs.addToRenderer(self.ren)
            cnf.add(obs)



    def selectObserverItem(self):
        # return 
        # print 'selected item'
        for observerClass in self.elems:
            for obs in observerClass.elems:
                if hasattr(obs,'update'):
                    obs.update(color=[0.1,0.1,0.9])

        wdgt = self.obsw.selectedItems()[0]
        if hasattr(wdgt,'update'):
            wdgt.update(color=[0.1,0.9,0.1])
        self.repaint()
        




class ObserverCircleDialog(QtGui.QDialog):

    def __init__(self,parent=None,**kwargs):
        super(ObserverCircleDialog,self).__init__()
        self.ui = Ui_ObserverPlaneCircleDialog()
        self.ui.setupUi(self)

        self._connectToConfig()

    def displayItem(self,item):
        self.currentItem = item
        self.conf.fromstr(item.getValues())

    def _connectToConfig(self):
        # TODO: Markers
        self.conf = pcConfigSection('pcObserverCircle')
        self.conf.add(pcConfigEntry('SetMode',QtcomboBoxInteractor(self.ui.ObserverPlaneMode,{"sensor":0,"detector":1})))
        self.conf.add(pcConfigEntry('SetOrigin',QtvectorInteractor(self.ui.ObserverOrigin_X,self.ui.ObserverOrigin_Y,self.ui.ObserverOrigin_Z)))
        self.conf.add(pcConfigEntry('SetNormal',QtvectorInteractor(self.ui.ObserverNormal_X,self.ui.ObserverNormal_Y,self.ui.ObserverNormal_Z)))
        self.conf.add(pcConfigEntry('SetRadius',QtspinBoxInteractor(self.ui.ObserverCircleRadius)))
        self.conf.add(pcConfigEntry('SaveIntermediateSteps',QtcheckBoxInteractor(self.ui.SaveIntermediateSteps,{'true':True,'false':False})))
        self.conf.add(pcConfigEntry('SetOutputFileName',QtlineEditInteractor(self.ui.ObserverPlaneOutputFileName)))
        self.conf.add(pcConfigEntry('SetDescription',QtlineEditInteractor(self.ui.ObserverDescription)))


    def accept(self):
        print "accepted"
        self.currentItem.setValues(self.conf.todict().popitem()[1])
        self.currentItem.update()
        super(ObserverCircleDialog,self).accept()

    def reject(self):
        print "LOG: ObserverCircleDialog rejected"
        super(ObserverCircleDialog,self).reject()


class ObserverRectangleDialog(QtGui.QDialog):

    def __init__(self,parent=None,**kwargs):
        super(ObserverRectangleDialog,self).__init__()
        self.ui = Ui_ObserverRectangleDialog()
        self.ui.setupUi(self)

        self._connectToConfig()

    def displayItem(self,item):
        self.currentItem = item
        self.conf.fromstr(item.getValues())

    def _connectToConfig(self):
        # TODO: Markers
        self.conf = pcConfigSection('pcObserverRectangle')
        self.conf.add(pcConfigEntry('SetMode',QtcomboBoxInteractor(self.ui.ObserverPlaneMode,{"sensor":0,"detector":1})))
        self.conf.add(pcConfigEntry('SetOrigin',QtvectorInteractor(self.ui.ObserverOrigin_X,self.ui.ObserverOrigin_Y,self.ui.ObserverOrigin_Z)))
        self.conf.add(pcConfigEntry('SetNormal',QtvectorInteractor(self.ui.ObserverNormal_X,self.ui.ObserverNormal_Y,self.ui.ObserverNormal_Z)))
        # self.conf.add(pcConfigEntry('SetRadius',QtspinBoxInteractor(self.ui.ObserverCircleRadius)))
        self.conf.add(pcConfigEntry('SetSideA',QtspinBoxInteractor(self.ui.SideA)))
        self.conf.add(pcConfigEntry('SetSideB',QtspinBoxInteractor(self.ui.SideB)))
        self.conf.add(pcConfigEntry('SetRectangleNorth',QtvectorInteractor(self.ui.RectangleNorth_X,self.ui.RectangleNorth_Y,self.ui.RectangleNorth_Z)))
        self.conf.add(pcConfigEntry('SaveIntermediateSteps',QtcheckBoxInteractor(self.ui.SaveIntermediateSteps,{'true':True,'false':False})))
        self.conf.add(pcConfigEntry('SetOutputFileName',QtlineEditInteractor(self.ui.ObserverPlaneOutputFileName)))
        self.conf.add(pcConfigEntry('SetDescription',QtlineEditInteractor(self.ui.ObserverDescription)))


    def accept(self):
        print "accepted"
        self.currentItem.setValues(self.conf.todict().popitem()[1])
        self.currentItem.update()
        super(ObserverRectangleDialog,self).accept()

    def reject(self):
        print "LOG: ObserverRectangleDialog rejected"
        super(ObserverRectangleDialog,self).reject()

class StructuredFluenceDialog(QtGui.QDialog):

    def __init__(self,parent=None,**kwargs):
        super(StructuredFluenceDialog,self).__init__()
        self.ui = Ui_StructuredFluenceObserverDialog()
        self.ui.setupUi(self)

        

        if 'vtkimagefile' in kwargs:
            self.vtkimagefile = kwargs['vtkimagefile']
        else:
            self.vtkimagefile = None
        self._connectToConfig()

    def displayItem(self,item):
        self.currentItem = item
        self.conf.fromstr(item.getValues())

    def _connectToConfig(self):
        self.conf = pcConfigSection('StructuredFluence')
        if self.vtkimagefile != None:
            self.conf.add(pcConfigEntry('SetVtkImageDataObject',self.vtkimagefile))
        self.conf.add(pcConfigEntry('SaveIntermediateSteps',QtcheckBoxInteractor(self.ui.SaveIntermediateSteps,{'true':True,'false':False})))
        self.conf.add(pcConfigEntry('SetDescription',QtlineEditInteractor(self.ui.Description)))
        self.conf.add(pcConfigEntry('SetOutputFileName',QtlineEditInteractor(self.ui.StructuredFluenceOutputFileName)))
        self.conf.add(pcConfigEntry('InitDefaults',QtspinBoxInteractor(self.ui.InitialValue)))

    def accept(self):
        print "accepted"
        self.currentItem.setValues(self.conf.todict().popitem()[1])
        self.currentItem.update()
        super(StructuredFluenceDialog,self).accept()

    def reject(self):
        print "LOG: StructuredFluenceDialog rejected"
        super(StructuredFluenceDialog,self).reject()


class GeometryDisplayOptionsDialog(QtGui.QDialog):

    def __init__(self,parent=None,**kwargs):
        super(GeometryDisplayOptionsDialog,self).__init__()
        self.ui = Ui_GeometryDisplayOptions()
        self.ui.setupUi(self)

        

        # if 'vtkimagefile' in kwargs:
        #     self.vtkimagefile = kwargs['vtkimagefile']
        # else:
        #     self.vtkimagefile = None
        # self._connectToConfig()

    # def displayItem(self,item):
    #     self.currentItem = item
    #     self.conf.fromstr(item.getValues())

    # def _connectToConfig(self):
    #     self.conf = pcConfigSection('StructuredFluence')
    #     if self.vtkimagefile != None:
    #         self.conf.add(pcConfigEntry('SetVtkImageDataObject',self.vtkimagefile))
    #     self.conf.add(pcConfigEntry('SaveIntermediateSteps',QtcheckBoxInteractor(self.ui.SaveIntermediateSteps,{'true':True,'false':False})))
    #     self.conf.add(pcConfigEntry('SetDescription',QtlineEditInteractor(self.ui.Description)))
    #     self.conf.add(pcConfigEntry('SetOutputFileName',QtlineEditInteractor(self.ui.StructuredFluenceOutputFileName)))
    #     self.conf.add(pcConfigEntry('InitDefaults',QtspinBoxInteractor(self.ui.InitialValue)))

        self.interact = Struct()
        self.interact.wireframe = QtcheckBoxInteractor(self.ui.Wireframe,{'true':True,'false':False})
        self.interact.threshold = QtcheckBoxInteractor(self.ui.Threshold,{'true':True,'false':False})
        self.interact.thresholdLower = QtspinBoxInteractor(self.ui.ThresholdLower)
        self.interact.thresholdUpper = QtspinBoxInteractor(self.ui.ThresholdUpper)

    def accept(self):
        print "accepted"
        # self.currentItem.setValues(self.conf.todict().popitem()[1])
        # self.currentItem.update()
        super(GeometryDisplayOptionsDialog,self).accept()

    def reject(self):
        print "LOG: GeometryDisplayOptionsDialog rejected"
        super(GeometryDisplayOptionsDialog,self).reject()

    



class Application(QtGui.QMainWindow):

    class VtkSource(object):

        def __init__(self,parent=None):
            self.parent = parent
            self.source = Struct()
            self.source.orig = QtvectorInteractor(parent.ui.SourceOrigin_X,parent.ui.SourceOrigin_Y,parent.ui.SourceOrigin_Z)
            self.source.outputDir = QtvectorInteractor(parent.ui.OutputDirection_X,parent.ui.OutputDirection_Y,parent.ui.OutputDirection_Z)
            self.source.isotropic = QtcheckBoxInteractor(parent.ui.IsotropicSource,{'pcPhotonPointSource':False, 'pcIsotropicPhotonPointSource':True})
            # print self.source.isotropic.get()
            self.source.vtkarr = []
            
        def refresh(self):

            if len(self.source.vtkarr) > 0:
                for elem in self.source.vtkarr:
                    elem.removeFromRenderer(self.parent.ren)
                self.source.vtkarr = []

            if self.source.isotropic.get() == 'pcIsotropicPhotonPointSource':
                print 'isotropicsource'
                for i in range(0,3):
                # self.source.vtkarr = [] 
                    vec1 = np.array([0.0,0.0,0.0])
                    vec1[i] = 1.0
                    vec2 = vec1.copy()
                    vec2[i] = -1.0
                    # print vec1
                    # print vec2
                    self.source.vtkarr.append(VtkArrow(origin=self.source.orig.todict(),outputDirection=vec1.copy(),scale=4,noinvert=1))
                    self.source.vtkarr.append(VtkArrow(origin=self.source.orig.todict(),outputDirection=vec2.copy(),scale=4,noinvert=1))
                for elem in self.source.vtkarr:
                    elem.addToRenderer(self.parent.ren)

                print 'now we have {} arrows'.format(len(self.source.vtkarr))
            else:
                # print self.source.orig.todict()
                self.source.vtkarr.append(VtkArrow(origin=self.source.orig.todict(),outputDirection=self.source.outputDir.todict()))
                self.source.vtkarr[0].addToRenderer(self.parent.ren)


    class VtkGeometry(object):
        def __init__(self,parent=None,**kwargs):
            self.parent = parent
            self.geo = Struct()
            self.manual = QtcheckBoxInteractor(parent.ui.ManualGeometry,{'pcRegularGridGeometry':False,'pcHomogeneousRegularGridGeometry':True})
            self.homogeneous = QtcheckBoxInteractor(parent.ui.HomogeneousMaterial,{'pcRegularGridGeometry':False,'pcHomogeneousRegularGridGeometry':True})

            self.vtkfile = QtlineEditInteractor(parent.ui.VTKImageFile)

            self.spacing = QtvectorInteractor(parent.ui.Spacing_X,parent.ui.Spacing_Y,parent.ui.Spacing_Z)
            self.orig = QtvectorInteractor(parent.ui.GeometryOrigin_X,parent.ui.GeometryOrigin_Y,parent.ui.GeometryOrigin_Z)
            self.dimensions = QtvectorInteractor(parent.ui.Dimension_X,parent.ui.Dimension_Y,parent.ui.Dimension_Z)

            self.mua = QtspinBoxInteractor(parent.ui.Mua)
            self.mus = QtspinBoxInteractor(parent.ui.Mus)
            self.g = QtspinBoxInteractor(parent.ui.g)

            self.GeometryDisplayOptions = kwargs['interact']
            self.GeometryDisplayOptions.wireframe.set('true')
            self.GeometryDisplayOptions.threshold.set('false')
            # self.GeometryDisplayOptions.wireframe = 'true'
            # self.GeometryDisplayOptions.threshold = 'false'
            # self.GeometryDisplayOptions.thresholdLower = -1.0
            # self.GeometryDisplayOptions.thresholdUpper = -1.0


        def refresh(self,**kwargs):

            if self.homogeneous.get() == 'pcRegularGridGeometry':
                if not os.path.isfile(self.vtkfile.get()):
                    print 'ERR: can\'t find the specified vtk file!'
                    return
                else:
                    reader = vtk.vtkXMLImageDataReader()
                    reader.SetFileName(self.vtkfile.get())
                    self.img = reader.GetOutput()
                    reader.Update()
                    # print dir(self.img)
                    self.dimensions.set(self.img.GetDimensions())
                    self.orig.set(self.img.GetOrigin())
                    self.spacing.set(self.img.GetSpacing())

                    self.mapperPort = reader.GetOutputPort()
                    

                    # self.mapper = vtk.vtkDataSetMapper()
                               
                    # #if VTK_MAJOR_VERSION <= 5
                    #   mapper->SetInputConnection(imageData->GetProducerPort());
                    # #else
                    #   mapper->SetInputData(imageData);
                    # #endif            
                    
                    # self.mapper.SetInputConnection(reader.GetOutputPort())

                    
            else:
                self.img = vtk.vtkImageData()

                # Note: The dimensions specified in the gui are CELL-Dimensions!!!
                # Hence, in order to display the correct wireframe, we need to add +1 in each direction
                # cause SetDimensions sets the point dimensions!
                dims = self.dimensions.todict()
                self.img.SetDimensions(dims[0]+1,dims[1]+1,dims[2]+1)
                self.img.SetSpacing(self.spacing.todict())
                self.img.SetOrigin(self.orig.todict())
                self.mapperPort = self.img.GetProducerPort()

            self.threshold = vtk.vtkThreshold()
            # self.threshold.SetAttributeModeToUseCellData()
            self.threshold.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, "material_label_map")
            # vtkDataObject.FIELD_ASSOCIATION_CELLS, vtkDataSetAttributes.SCALARS
            self.mapper = vtk.vtkDataSetMapper()
            if self.GeometryDisplayOptions.threshold.get() == 'true':
                print 'threshold for image data... between {} and {}'.format(float(self.GeometryDisplayOptions.thresholdLower.get()),float(self.GeometryDisplayOptions.thresholdUpper.get()))
                self.threshold.ThresholdBetween( 
                        float(self.GeometryDisplayOptions.thresholdLower.get()), 
                        float(self.GeometryDisplayOptions.thresholdUpper.get())
                    )
                self.threshold.SetInputConnection(self.mapperPort)
                self.mapper.SetInputConnection(self.threshold.GetOutputPort())
            else:            
                self.mapper.SetInputConnection(self.mapperPort)

            actor = vtk.vtkActor()
            actor.SetMapper(self.mapper)
            # actor.
            # print dir(actor)
            actor.GetProperty().SetColor(1.0,1.0,1.0)
            if self.GeometryDisplayOptions.wireframe.get() == 'true':
                actor.GetProperty().SetRepresentationToWireframe()
            elif self.GeometryDisplayOptions.threshold.get() == 'true':
                actor.GetProperty().SetRepresentationToSurface()
                actor.GetProperty().SetOpacity(0.3)
                # actor.GetProperty().SetDiffuseColor(1,1,1)
            else:
                pass
            # actor.GetProperty().EdgeVisibilityOn()
            
            
            actor.GetProperty().SetAmbient(1)
            actor.GetProperty().SetDiffuse(0)
            actor.GetProperty().SetSpecular(0)
            

            self.parent.ren.AddActor(actor) 

            self.parent.ren.GetActiveCamera().SetFocalPoint(self.dimensions.todict()[0]/2*self.spacing.todict()[0]+self.orig.todict()[0], \
                                                            self.dimensions.todict()[1]/2*self.spacing.todict()[1]+self.orig.todict()[1], \
                                                            self.dimensions.todict()[2]/2*self.spacing.todict()[2]+self.orig.todict()[2]
                                                        )  # Set/Get the focal of the camera in world coordinates. The default focal point is the origin.


    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        self.parent = parent
        self.conf = Struct()
        self.qti = Struct()
        self.conf.root = pcConfigSection('root')

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        
        self.ui.qvtkWidget.Initialize()
        self.ui.qvtkWidget.Start()
        # if you dont want the 'q' key to exit comment this.
        self.ui.qvtkWidget.AddObserver("ExitEvent", lambda o, e, a=app: a.quit())

        self.ren = vtk.vtkRenderer()
        self.ui.qvtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.axes = VtkAxes()
        self.axes_shown = False
        self.toggle_axes()
        
        style = vtk.vtkInteractorStyleTrackballCamera()
        self.ui.qvtkWidget.SetInteractorStyle(style)
        self.ui.qvtkWidget.show()


        self.setupObserverList()
        self.setupUiSignals()

        self.GeometryDisplayOptionsDialog = GeometryDisplayOptionsDialog()

        self.source = self.VtkSource(self)
        self.geometry = self.VtkGeometry(self,interact=self.GeometryDisplayOptionsDialog.interact)

        


        self.setInitialValues()
        self.connectUiToConfig()

    def setupObserverList(self):
        self.ui.ObserverList.setColumnWidth(0,200)
        self.ui.ObserverList.setColumnWidth(1,100)
        

    def refresh(self):
        print 'refresh vtk widget'

        
        self.source.refresh()
        self.geometry.refresh()
        self.repaint()


    def repaint(self):
        print 'LOG: repainting vtk widget'
        self.ui.qvtkWidget.repaint()
        


            

    def setupUiSignals(self):
        QtCore.QObject.connect(self.ui.actionSave_config, QtCore.SIGNAL(_fromUtf8("triggered()")), self.save_config)
        QtCore.QObject.connect(self.ui.actionOpen_config, QtCore.SIGNAL(_fromUtf8("triggered()")), self.load_config)
        QtCore.QObject.connect(self.ui.AddObserverButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.addObserver)
        QtCore.QObject.connect(self.ui.ChooseVTKFileButton, QtCore.SIGNAL("clicked()"), self.select_vtk_file)
        QtCore.QObject.connect(self.ui.ChooseOutputDirectoryButton, QtCore.SIGNAL("clicked()"), self.choose_output_dir)
        QtCore.QObject.connect(self.ui.ChooseMaterialDBButton, QtCore.SIGNAL("clicked()"), self.choose_material_db)
        QtCore.QObject.connect(self.ui.actionOptions, QtCore.SIGNAL(_fromUtf8("triggered()")), self.edit_geometry_display_options)
        
        
        QtGui.QShortcut(QtGui.QKeySequence.Refresh, self, self.refresh)
        QtGui.QShortcut(QtGui.QKeySequence("r"), self, self.refresh)
        QtGui.QShortcut(QtGui.QKeySequence("o"), self, self.load_config)
        QtGui.QShortcut(QtGui.QKeySequence("s"), self, self.save_config)
        QtGui.QShortcut(QtGui.QKeySequence("w"), self, QtGui.QApplication.exit)
        QtGui.QShortcut(QtGui.QKeySequence("a"), self, self.toggle_axes)
        QtGui.QShortcut(QtGui.QKeySequence("c"), self, self.edit_geometry_display_options)
        QtGui.QShortcut(QtGui.QKeySequence("p"), self, self.save_scene_to_png)

    def setInitialValues(self):
        self.ui.DebugLevel.setCurrentIndex(4) # INFO
        self.ui.RNG.setCurrentIndex(2) # mat19937
        self.ui.PhotonWeight.setValue(100.0)

    def connectUiToConfig(self):
        self.conf.root.add(pcConfigEntry('DebugLevel',QtcomboBoxInteractor(self.ui.DebugLevel,{'DEBUG4':0,'DEBUG3':1,'DEBUG2':2,'DEBUG1':3,'INFO':4,'WARNING':5,'ERROR':6})))
        self.conf.root.add(pcConfigEntry('ResultDir',QtlineEditInteractor(self.ui.OutputDirectory)))
        self.conf.root.add(pcConfigEntry('MaterialDB',QtlineEditInteractor(self.ui.MaterialDB)))
        
        self.conf.RNG = pcConfigSection('RNG')
        self.conf.root.add(self.conf.RNG)
        self.conf.RNG.add(pcConfigEntry('GslRngType',QtcomboBoxInteractor(self.ui.RNG,{'taus':0,'gfsr4':1,'mt19937':2,'ranlxs0':3,'ranlxs1':4,'mrg':5,'ranlux':6,'ranlxd1':7,'ranlxd2':8,'dmrg':9,'ranlux389':10,'ranlxd2':11})))
        self.conf.RNG.add(pcConfigEntry('GslRngSeed','0'))

        self.conf.MCSimulator = pcConfigSection('MCSimulator')
        self.conf.root.add(self.conf.MCSimulator)
        self.conf.MCSimulator.add(pcConfigEntry('NumberOfPhotons',QtlineEditInteractor(self.ui.NrOfPhotons)))
        self.conf.MCSimulator.add(pcConfigEntry('MonteCarloMethod',QtcomboBoxInteractor(self.ui.MonteCarloMethod,{'pcPhotonPacketVariableStepsize':0,'pcSinglePhotonFixedStepsize':1,'pcSinglePhotonVariableStepsize':2,'pcPhotonPacketVariableStepsizeStepping':3,'pcPhotonPacketVariableStepsizeStepping2':4})))

        self.conf.MCMethod = pcConfigSection('MCMethod')
        self.conf.MCSimulator.add(self.conf.MCMethod)
        self.conf.PhotonPacketStepping = pcConfigSection('pcPhotonPacketVariableStepsizeStepping2')
        self.conf.MCMethod.add(self.conf.PhotonPacketStepping)
        self.conf.PhotonPacketStepping.add(pcConfigEntry('SetWeightThreshold',QtspinBoxInteractor(self.ui.WeightThreshold)))
        self.conf.PhotonPacketStepping.add(pcConfigEntry('SetRouletteChance',QtspinBoxInteractor(self.ui.RouletteChance)))

        # The Geometry section:
        self.conf.Geometry = pcConfigSection('Geometry')
        self.conf.MCSimulator.add(self.conf.Geometry)
        self.conf.Geometry.add(pcConfigEntry('type',QtcheckBoxInteractor(self.ui.HomogeneousMaterial,{'pcRegularGridGeometry':False,'pcHomogeneousRegularGridGeometry':True})))
        
        self.conf.HomogeneousRegularGridGeometry = pcConfigSection('pcHomogeneousRegularGridGeometry')
        self.conf.Geometry.add(self.conf.HomogeneousRegularGridGeometry)
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('SetOrigin',QtvectorInteractor(self.ui.GeometryOrigin_X,self.ui.GeometryOrigin_Y,self.ui.GeometryOrigin_Z)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('SetSpacing',QtvectorInteractor(self.ui.Spacing_X,self.ui.Spacing_Y,self.ui.Spacing_Z)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('SetDimensions',QtvectorInteractor(self.ui.Dimension_X,self.ui.Dimension_Y,self.ui.Dimension_Z)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('mu_a', QtspinBoxInteractor(self.ui.Mua)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('mu_s', QtspinBoxInteractor(self.ui.Mus)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('g', QtspinBoxInteractor(self.ui.g)))
        self.conf.HomogeneousRegularGridGeometry.add(pcConfigEntry('n', QtspinBoxInteractor(self.ui.n)))

        self.conf.RegularGridGeometry = pcConfigSection('pcRegularGridGeometry')
        self.conf.Geometry.add(self.conf.RegularGridGeometry)
        self.conf.RegularGridGeometry.add(pcConfigEntry('SetVtkImageDataObject',QtlineEditInteractor(self.ui.VTKImageFile)))


        # The Source section:
        self.conf.Sources = pcConfigSection('Sources')
        self.conf.MCSimulator.add(self.conf.Sources)
        self.conf.Sources.add(pcConfigEntry('type',QtcheckBoxInteractor(self.ui.IsotropicSource,{'pcPhotonPointSource':False, 'pcIsotropicPhotonPointSource':True})))
        self.conf.PointSource = pcConfigSection('pcPhotonPointSource')
        self.conf.Sources.add(self.conf.PointSource)
        self.conf.PointSource.add(pcConfigEntry('SetOrigin',QtvectorInteractor(self.ui.SourceOrigin_X,self.ui.SourceOrigin_Y,self.ui.SourceOrigin_Z)))
        self.conf.PointSource.add(pcConfigEntry('SetOutputDirection',QtvectorInteractor(self.ui.OutputDirection_X,self.ui.OutputDirection_Y,self.ui.OutputDirection_Z)))
        self.conf.PointSource.add(pcConfigEntry('SetNumericalAperture',QtspinBoxInteractor(self.ui.NumericalApperture)))
        self.conf.PointSource.add(pcConfigEntry('SetInitialPhotonWeight',QtspinBoxInteractor(self.ui.PhotonWeight)))

        self.conf.IsotropicPointSource = pcConfigSection('pcIsotropicPhotonPointSource')
        self.conf.Sources.add(self.conf.IsotropicPointSource)
        self.conf.IsotropicPointSource.add(pcConfigEntry('SetOrigin',QtvectorInteractor(self.ui.SourceOrigin_X,self.ui.SourceOrigin_Y,self.ui.SourceOrigin_Z)))
        self.conf.IsotropicPointSource.add(pcConfigEntry('SetInitialPhotonWeight',QtspinBoxInteractor(self.ui.PhotonWeight)))

        # The Observer section:
        self.conf.Observers = ObserverTreeWidgetHandler('Observers',self.ui.ObserverList,self.ren,self.repaint,QtlineEditInteractor(self.ui.VTKImageFile))
        self.conf.root.add(self.conf.Observers)


    def addObserver(self):

        self.conf.Observers.addObserver(self.ui.AddObserverType.currentText())

    # #####################################################################################
    # Commands 
    # #####################################################################################

    def save_config(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save config file', './', "Config Files (*.cnf)")
        with open(filename,'w') as f:
            f.write(self.conf.root.tostr())
        
        print self.conf.root.tostr()


    def load_config(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open file', './', "Config Files (*.cnf)")
        if filename != '':
            
            # pythonlibdir = get_python_lib()
            confspecfile = resource_filename('photoncruncher','data/pcConfig.ini')
    
            if not os.path.isfile(confspecfile):
                raise Exception('Confspecfile not found: '+confspecfile)
            

            
            conf = ConfigObj(str(filename),configspec=confspecfile,indent_type='\t')
            val = Validator()
            result = conf.validate(val, preserve_errors=False, copy=True)   
            
            self.conf.root.fromstr(conf)

    def edit_geometry_display_options(self):
        self.GeometryDisplayOptionsDialog.exec_()


    def select_vtk_file(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open vti file', './', "VTK Image Data (*.vti)")
        self.ui.VTKImageFile.setText(filename)

    def choose_output_dir(self):
        outdir = QtGui.QFileDialog.getExistingDirectory(self, 'Select Result Directory', './')
        self.ui.OutputDirectory.setText(outdir)

    def choose_material_db(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Select Material Database', './', "Material DB file (*.db)")
        self.ui.MaterialDB.setText(filename)

    def toggle_axes(self):
        if not self.axes_shown:
            self.axes.addToRenderer(self.ren)
            self.axes_shown = True
        else:
            self.axes.removeFromRenderer(self.ren)
            self.axes_shown = False    
        self.repaint()
        

    def save_scene_to_png(self):
        filter = vtk.vtkWindowToImageFilter()
        filter.SetInput(self.ren.GetVTKWindow())
        filter.Modified()
        writer = vtk.vtkPNGWriter()
        writer.SetInput(filter.GetOutput());
        writer.SetFileName('vtkScene.png');
        writer.Write()
        
    # #####################################################################################
    # Commands 
    # #####################################################################################











class VtkArrow(object):
    def __init__(self,*args,**kwargs):
        super(VtkArrow,self).__init__()

        if not 'origin' in kwargs:
            self.origin = [0.0,0.0,0.0]
        else:
            self.origin = kwargs['origin']

        if not 'outputDirection' in kwargs:
            self.outputDirection = [1.0,0.0,0.0]
        else:
            self.outputDirection = kwargs['outputDirection']

        if not 'scale' in kwargs:
            self.scale = 4
        else:
            self.scale = kwargs['scale']

        if not 'noinvert' in kwargs:
            self.invert = True
        else:
            self.invert = False

        self.arrowSource = vtk.vtkArrowSource()

        if self.invert:
            self.arrowSource.InvertOn()
        self.arrowSource.SetShaftResolution(12)
        self.arrowSource.SetTipResolution(12)


        self.matrix = vtk.vtkMatrix4x4()
        
        self.transformPD = vtk.vtkTransformPolyDataFilter()
        self.arrowMapper = vtk.vtkPolyDataMapper()
        self.actor = vtk.vtkActor()

        self.update()


    def update(self,**kwargs):

        if 'origin' in kwargs:
            self.origin = kwargs['origin']

        if 'outputDirection' in kwargs:
            self.outputDirection = kwargs['outputDirection']

        if 'scale' in kwargs:
            self.scale = kwargs['scale']
        
        self.matrix.Identity()
        invert = lambda vec: map(lambda x : -x, vec)
        normalize = lambda vec: map(lambda x: x/np.sqrt(vec[0]**2+vec[1]**2+vec[2]**2),vec)
        xaxis = invert(normalize(self.outputDirection)) # element-wise negation, could be improved by using np
        yaxis = normalize([1.0,1.0,1.0])
        zaxis = np.cross(xaxis,yaxis)
        yaxis = np.cross(zaxis,normalize(self.outputDirection))

        for i in range(0,3):
            self.matrix.SetElement(i,0,xaxis[i]) # normalized X direction (of the arrow)
            self.matrix.SetElement(i,1,yaxis[i]) # normalized Y direction (of the arrow)
            self.matrix.SetElement(i,2,zaxis[i]) # normalized Z direction (of the arrow)

        self.transform = vtk.vtkTransform()
        self.transform.Translate(self.origin)
        self.transform.Concatenate(self.matrix)
        self.transform.Scale(self.scale,self.scale,self.scale)

        
        self.transformPD.SetTransform(self.transform)
        self.transformPD.SetInputConnection(self.arrowSource.GetOutputPort())

        
        self.arrowMapper.SetInputConnection(self.transformPD.GetOutputPort())

        
        self.actor.GetProperty().SetColor(0.9,0.1,0.1)
        self.actor.SetMapper(self.arrowMapper)

    def addToRenderer(self,ren):
        ren.AddActor(self.actor)

    def removeFromRenderer(self,ren):
        ren.RemoveActor(self.actor)


class VtkPlaneCircle(object):
    def __init__(self,*args,**kwargs):
                    
        if not 'radius' in kwargs:
            self.radius = 10
            self.border = False
        else:
            self.radius = kwargs['radius']
            self.border = True

        if not 'origin' in kwargs:
            self.origin = [0,0,0]
        else:
            self.origin = kwargs['origin']

        if not 'normal' in kwargs:
            self.normal = [1,0,0]
        else:
            self.normal = kwargs['normal']

        if not 'color' in kwargs:
            self.color = [0.1,0.1,0.9]
        else:
            self.color = kwargs['color']

        self.polySource = vtk.vtkRegularPolygonSource()


        self.surfaceMapper = vtk.vtkPolyDataMapper()
        self.surfaceMapper.SetInputConnection(self.polySource.GetOutputPort())
        

        self.surfaceActor = vtk.vtkActor()
        
        
        self.surfaceActor.GetProperty().SetRepresentationToSurface();
        
        

        if self.border:
            self.circleActor = vtk.vtkActor()


        self.update()


    def update(self,*args,**kwargs):

        if 'radius' in kwargs:
            self.radius = kwargs['radius']
            self.border = True

        if 'origin' in kwargs:
            self.origin = kwargs['origin']

        if 'normal' in kwargs:
            self.normal = kwargs['normal']

        if 'color' in kwargs:
            self.color = kwargs['color']


        if not self.border:
            self.polySource.GeneratePolylineOff()

        self.polySource.SetNumberOfSides(50)

        self.polySource.SetRadius(self.radius);
        self.polySource.SetCenter(self.origin[0], self.origin[1], self.origin[2]);
        self.polySource.SetNormal(self.normal);

        self.surfaceActor.SetMapper(self.surfaceMapper)
        self.surfaceActor.GetProperty().SetColor(self.color)
        self.surfaceActor.GetProperty().SetOpacity(0.4)

        if self.border:
            self.tubes = vtk.vtkTubeFilter()
            self.tubes.SetInputConnection(self.polySource.GetOutputPort());
            self.tubes.SetNumberOfSides(12);
            self.tubes.SetRadius(.1);

            self.circleMapper = vtk.vtkPolyDataMapper()
            self.circleMapper.SetInputConnection(self.tubes.GetOutputPort());

            self.circleActor.SetMapper(self.circleMapper);
            self.circleActor.GetProperty().SetColor(self.color);


    def addToRenderer(self,ren):
        ren.AddActor(self.surfaceActor)
        if self.border:
            ren.AddActor(self.circleActor)


    def removeFromRenderer(self,ren):
        ren.RemoveActor(self.surfaceActor)
        if self.border:
            ren.RemoveActor(self.circleActor)





class VtkRectangle(object):
    def __init__(self,*args,**kwargs):


        if not 'origin' in kwargs:
            self.origin = [0,0,0]
        else:
            self.origin = kwargs['origin']

        if not 'normal' in kwargs:
            self.normal = [0,0,1]
        else:
            self.normal = kwargs['normal']

        if not 'color' in kwargs:
            self.color = [0.1,0.1,0.9]
        else:
            self.color = kwargs['color']

        if not 'sideA' in kwargs:
            self.sideA = 1
        else:
            self.sideA = kwargs['sideA']

        if not 'sideB' in kwargs:
            self.sideB = 1
        else:
            self.sideB = kwargs['sideB']

        if not 'rectangleNorth' in kwargs:
            self.rectangleNorth = [1,0,0]
        else:
            self.rectangleNorth = kwargs['rectangleNorth']

        # create source
        self.source = vtk.vtkPlaneSource()

        # mapper
        self.surfaceMapper = vtk.vtkPolyDataMapper()
        self.surfaceMapper.SetInput(self.source.GetOutput())
         
        # actor
        self.surfaceActor = vtk.vtkActor()
        self.surfaceActor.SetMapper(self.surfaceMapper)
        
        self.edges = vtk.vtkExtractEdges()
        self.edges.SetInputConnection(self.source.GetOutputPort())

        self.tubes = vtk.vtkTubeFilter()
        self.tubes.SetInputConnection(self.edges.GetOutputPort())

        self.borderMapper = vtk.vtkPolyDataMapper()
        self.borderMapper.SetInputConnection(self.tubes.GetOutputPort())

        
        self.borderActor = vtk.vtkActor()
        self.borderActor.SetMapper(self.borderMapper)
        
        self.update()

    def update(self,*args,**kwargs):

        if 'color' in kwargs:
            self.color = kwargs['color']
        
        if 'origin' in kwargs:
            self.origin = kwargs['origin']

        if 'normal' in kwargs:
            self.normal = kwargs['normal']

        if 'sideA' in kwargs:
            self.sideA = kwargs['sideA']

        if 'sideB' in kwargs:
            self.sideB = kwargs['sideB']

        if 'rectangleNorth' in kwargs:
            self.rectangleNorth = kwargs['rectangleNorth']

        normalize = lambda vec: map(lambda x: x/np.sqrt(vec[0]**2+vec[1]**2+vec[2]**2),vec)

        self.source.SetOrigin(0,0,0)
        self.source.SetPoint1(self.sideB*np.array(normalize(self.rectangleNorth)))
        self.source.SetPoint2(self.sideA*np.array(normalize(np.cross(self.rectangleNorth,-np.array(self.normal)))))
        self.source.SetCenter(self.origin)
        self.source.SetNormal(self.normal)

        self.surfaceActor.GetProperty().SetColor(self.color)
        self.surfaceActor.GetProperty().SetOpacity(0.4)


        
        self.tubes.SetNumberOfSides(12)
        self.tubes.SetRadius(.1)

        self.borderActor.GetProperty().SetColor(self.color)

    def addToRenderer(self,ren):
        ren.AddActor(self.surfaceActor)
        ren.AddActor(self.borderActor)


    def removeFromRenderer(self,ren):
        ren.RemoveActor(self.surfaceActor)
        ren.RemoveActor(self.borderActor)
     

class VtkAxes(object):

    def __init__(self):
        self.axes = vtk.vtkAxesActor()
        
        self.axes.SetShaftTypeToCylinder()
        self.axes.SetXAxisLabelText("x")
        self.axes.GetXAxisShaftProperty().SetColor(1,0.26,0.44)
        self.axes.GetXAxisTipProperty().SetColor(1,0.26,0.44)
        
        self.axes.SetYAxisLabelText("y")
        self.axes.GetYAxisShaftProperty().SetColor(0.50,1,0.62)
        self.axes.GetYAxisTipProperty().SetColor(0.50,1,0.62)
        
        self.axes.SetZAxisLabelText("z")
        self.axes.GetZAxisShaftProperty().SetColor(0.50,0.62,1)
        self.axes.GetZAxisTipProperty().SetColor(0.50,0.62,1)
        
        self.axes.SetTotalLength(2,2,2)
        
    #   self.axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleModeToViewport()
    #   self.axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleModeToViewport()
    #   self.axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleModeToViewport()
        
    #   self.axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleModeToProp()
    #   self.axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleModeToProp()
    #   self.axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleModeToProp()
        
        self.axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
        self.axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
        self.axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone() 
        
        self.axes.GetXAxisCaptionActor2D().GetTextActor().GetTextProperty().SetFontSize(30);
        self.axes.GetXAxisCaptionActor2D().GetTextActor().GetTextProperty().SetColor(1,1,1);
        self.axes.GetYAxisCaptionActor2D().GetTextActor().GetTextProperty().SetFontSize(30);
        self.axes.GetYAxisCaptionActor2D().GetTextActor().GetTextProperty().SetColor(1,1,1);
        self.axes.GetZAxisCaptionActor2D().GetTextActor().GetTextProperty().SetFontSize(30);
        self.axes.GetZAxisCaptionActor2D().GetTextActor().GetTextProperty().SetColor(1,1,1); 

    def update(self,**kwargs):
        pass 

    def addToRenderer(self,ren):
        ren.AddActor(self.axes)

    def removeFromRenderer(self,ren):
        ren.RemoveActor(self.axes)

if __name__ == "__main__":
    """A simple example that uses the QVTKRenderWindowInteractor class."""
    # every QT app needs an app
    argv = sys.argv
    app = QtGui.QApplication(argv)
    myapp = Application()
    myapp.show()
    sys.exit(app.exec_())

