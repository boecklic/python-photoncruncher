#!/bin/bash

# This is a script adapted from 
# http://calvinx.com/2012/11/03/ipython-qtconsole/
 
LIBS=( vtk PyQt4 sip.so )
PYTHON_VERSION=python$(python -c "import sys; print (str(sys.version_info[0])+'.'+str(sys.version_info[1]))")
VAR=( $(which -a $PYTHON_VERSION) )
GET_PYTHON_LIB_CMD="from distutils.sysconfig import get_python_lib; print (get_python_lib())"
LIB_VIRTUALENV_PATH=$(python -c "$GET_PYTHON_LIB_CMD")
LIB_SYSTEM_PATH=$(${VAR[1]} -c "$GET_PYTHON_LIB_CMD")
 
for LIB in ${LIBS[@]}
do
# Make each library specified in LIBS available in our current virtual env
# -s symbolic
# -i prompt if target exists
# -n don't follow if target is a symlink
	ln -sin $LIB_SYSTEM_PATH/$LIB $LIB_VIRTUALENV_PATH/$LIB
done