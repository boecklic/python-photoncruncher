#!/usr/bin/env python

import os
import stat

with open('photoncruncher.py','w') as f:
	f.write('#!/usr/bin/env python\n\n')

	f.write('from pctools import SingleRunner\n\n')

	f.write('SingleRunner()')

os.chmod('photoncruncher.py', stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)


with open('materials.db','w') as f:
	f.write('\
# the units of the coefficients are \n\
# mm, (mm^-1) \n\
\n\
\n\
[Vacuum] \n\
ID = 0 \n\
name = "Vacuum" \n\
mu_a = 0.0 \n\
mu_s = 0.0 \n\
g = 0.0 \n\
n = 1.0\n')


