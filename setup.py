#!/usr/bin/env python

from distutils.core import setup,Extension

setup(name='photoncruncher-tools',
	version='0.1.0',
	description='Tools for simpler usage of Monte Carlo Photon Simulator',
	author='Christoph Boecklin',
	author_email='christoph.boecklin@gmail.com',
	package_dir = {'':'src'},
	packages=['pctools', 'pctools.gui', 'mat'],
	package_data={'pctools': ['data/*'], 'pctools.gui': ['gui/data/*']},
	# package_data={'photoncruncher': ['_photoncruncher.*','data/*']},
	# ext_modules=[Extension('photoncruncher._pclib',['PhotonCruncher.i'],
	# 					swig_opts=['-c++','-classic','-I/Users/chris/usr/include/photoncruncher','-I/Users/chris/usr/include'],
	# 					include_dirs=['/Users/chris/usr/include/photoncruncher','/Users/chris/usr/include','/opt/local/include/vtk','/opt/local/include','/opt/local/include/eigen3'],
	# 					library_dirs=['/Users/chris/usr/lib'],
	# 					libraries=['photoncruncher']
	# 					)],
	# py_modules=['pclib'],
	scripts=[
			# 'bin/MonteCarloSimulator.py',
			# 'bin/mcs.py',
			'bin/setup_photoncruncher.py',
			'bin/photoncruncher-gui.py',
			'bin/photoncruncher-virtualenv-setup.sh',
		],
	install_requires=[
        	"configobj>=4.7.0",
        	"numpy>=1.7.0",
    	],
     )
