# -*- coding: utf-8 -*-


from pc import pcMaterial,pcMaterialDatabase_GetDB
from configobj import ConfigObj
from validate import Validator
import sys

def PopulateMaterialDatabase(matdbase):	

	mat = ConfigObj(matdbase+'.db',configspec=matdbase+'.spec',indent_type='\t')
	val = Validator()
	result = mat.validate(val,preserve_errors=False, copy=True)

#		print mat;
#		print result;
	if not result == True:
		print "Incorrect Material config: "
		for m in result:
			if not result[m] == True:
				print "Missing or incorrect value(s) in "+m+':'
				for n in result[m]:
					if not result[m][n] == True:
						print " -> "+n+" is missing";
						sys.exit(1)
	
	db = pcMaterialDatabase_GetDB();
	
	for m in mat:
		if not db.AddMaterial(\
					pcMaterial(mat[m]['ID'],\
							mat[m]['name'],\
							mat[m]['mu_a'],\
							mat[m]['mu_s'],\
							mat[m]['g'],\
							mat[m]['n'])):
			print "Error adding material: " + mat[m]['name'] + \
				"already exists."
		
	return mat