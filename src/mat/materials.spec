# Specification for the config-entry of a material in the material database
[__many__]
ID = integer()
name = string(max=30)
mu_a = float()
mu_s = float()
g = float()
n = float()


