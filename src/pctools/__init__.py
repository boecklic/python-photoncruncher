
from sys import platform as _platform
if _platform == "linux" or _platform == "linux2":
	import ctypes
	# Note: This is necessary to do here, otherwise there will be an
	# undefined symbol: mca_base_param_reg_int error
	ctypes.CDLL('libmpi.so', ctypes.RTLD_GLOBAL)



import os

from pclib import pcSignalHandler,InitMPI

## set up the signal handling mechanism to catch the Signal SIGUSR1 which 
## gracefully shuts the simulator down
sh = pcSignalHandler();

# This is to enable the console logging only for proces with rank 0
InitMPI()

# from pclib import *
from pcmcs import *




#__all__ = ["photoncruncher","mat"];
__libpath__ = os.path.realpath(__file__)
