# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt4-ui/GeometryDisplayOptions.ui'
#
# Created: Mon Aug 26 10:16:44 2013
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_GeometryDisplayOptions(object):
    def setupUi(self, GeometryDisplayOptions):
        GeometryDisplayOptions.setObjectName(_fromUtf8("GeometryDisplayOptions"))
        GeometryDisplayOptions.resize(719, 358)
        self.buttonBox = QtGui.QDialogButtonBox(GeometryDisplayOptions)
        self.buttonBox.setGeometry(QtCore.QRect(370, 320, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.Wireframe = QtGui.QCheckBox(GeometryDisplayOptions)
        self.Wireframe.setGeometry(QtCore.QRect(20, 10, 87, 20))
        self.Wireframe.setObjectName(_fromUtf8("Wireframe"))
        self.ThresholdLower = QtGui.QDoubleSpinBox(GeometryDisplayOptions)
        self.ThresholdLower.setEnabled(False)
        self.ThresholdLower.setGeometry(QtCore.QRect(170, 70, 62, 25))
        self.ThresholdLower.setMinimum(-1.0)
        self.ThresholdLower.setProperty("value", -1.0)
        self.ThresholdLower.setObjectName(_fromUtf8("ThresholdLower"))
        self.ThresholdUpper = QtGui.QDoubleSpinBox(GeometryDisplayOptions)
        self.ThresholdUpper.setEnabled(False)
        self.ThresholdUpper.setGeometry(QtCore.QRect(170, 100, 62, 25))
        self.ThresholdUpper.setMinimum(-1.0)
        self.ThresholdUpper.setProperty("value", -1.0)
        self.ThresholdUpper.setObjectName(_fromUtf8("ThresholdUpper"))
        self.label = QtGui.QLabel(GeometryDisplayOptions)
        self.label.setGeometry(QtCore.QRect(40, 70, 111, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(GeometryDisplayOptions)
        self.label_2.setGeometry(QtCore.QRect(40, 100, 111, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.Threshold = QtGui.QCheckBox(GeometryDisplayOptions)
        self.Threshold.setGeometry(QtCore.QRect(20, 40, 87, 20))
        self.Threshold.setObjectName(_fromUtf8("Threshold"))

        self.retranslateUi(GeometryDisplayOptions)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), GeometryDisplayOptions.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), GeometryDisplayOptions.reject)
        QtCore.QObject.connect(self.Threshold, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.ThresholdLower.setEnabled)
        QtCore.QObject.connect(self.Threshold, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), self.ThresholdUpper.setEnabled)
        QtCore.QMetaObject.connectSlotsByName(GeometryDisplayOptions)

    def retranslateUi(self, GeometryDisplayOptions):
        GeometryDisplayOptions.setWindowTitle(_translate("GeometryDisplayOptions", "Geometry Display Options", None))
        self.Wireframe.setText(_translate("GeometryDisplayOptions", "Wireframe", None))
        self.label.setText(_translate("GeometryDisplayOptions", "Threshold lower", None))
        self.label_2.setText(_translate("GeometryDisplayOptions", "Threshold upper", None))
        self.Threshold.setText(_translate("GeometryDisplayOptions", "Threshold", None))

