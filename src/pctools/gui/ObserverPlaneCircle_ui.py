# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt4-ui/ObserverPlaneCircle.ui'
#
# Created: Mon Aug 26 10:16:44 2013
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ObserverPlaneCircleDialog(object):
    def setupUi(self, ObserverPlaneCircleDialog):
        ObserverPlaneCircleDialog.setObjectName(_fromUtf8("ObserverPlaneCircleDialog"))
        ObserverPlaneCircleDialog.resize(561, 341)
        self.buttonBox = QtGui.QDialogButtonBox(ObserverPlaneCircleDialog)
        self.buttonBox.setGeometry(QtCore.QRect(200, 300, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.layoutWidget = QtGui.QWidget(ObserverPlaneCircleDialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 541, 282))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.ObserverPlaneMode = QtGui.QComboBox(self.layoutWidget)
        self.ObserverPlaneMode.setObjectName(_fromUtf8("ObserverPlaneMode"))
        self.ObserverPlaneMode.addItem(_fromUtf8(""))
        self.ObserverPlaneMode.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.ObserverPlaneMode, 1, 1, 1, 1)
        self.ObserverOrigin_X = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_X.setObjectName(_fromUtf8("ObserverOrigin_X"))
        self.gridLayout.addWidget(self.ObserverOrigin_X, 2, 1, 1, 1)
        self.ObserverDescription = QtGui.QLineEdit(self.layoutWidget)
        self.ObserverDescription.setObjectName(_fromUtf8("ObserverDescription"))
        self.gridLayout.addWidget(self.ObserverDescription, 0, 1, 1, 3)
        self.ObserverOrigin_Z = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_Z.setObjectName(_fromUtf8("ObserverOrigin_Z"))
        self.gridLayout.addWidget(self.ObserverOrigin_Z, 2, 3, 1, 1)
        self.ObserverPlaneMode_label = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneMode_label.setObjectName(_fromUtf8("ObserverPlaneMode_label"))
        self.gridLayout.addWidget(self.ObserverPlaneMode_label, 1, 0, 1, 1)
        self.ObserverCircleRadius = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverCircleRadius.setMinimum(-1.0)
        self.ObserverCircleRadius.setProperty("value", -1.0)
        self.ObserverCircleRadius.setObjectName(_fromUtf8("ObserverCircleRadius"))
        self.gridLayout.addWidget(self.ObserverCircleRadius, 4, 1, 1, 1)
        self.SourceOrigin_label = QtGui.QLabel(self.layoutWidget)
        self.SourceOrigin_label.setObjectName(_fromUtf8("SourceOrigin_label"))
        self.gridLayout.addWidget(self.SourceOrigin_label, 2, 0, 1, 1)
        self.OutputDirection_label = QtGui.QLabel(self.layoutWidget)
        self.OutputDirection_label.setObjectName(_fromUtf8("OutputDirection_label"))
        self.gridLayout.addWidget(self.OutputDirection_label, 3, 0, 1, 1)
        self.ObserverNormal_X = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_X.setObjectName(_fromUtf8("ObserverNormal_X"))
        self.gridLayout.addWidget(self.ObserverNormal_X, 3, 1, 1, 1)
        self.ObserverNormal_Y = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_Y.setObjectName(_fromUtf8("ObserverNormal_Y"))
        self.gridLayout.addWidget(self.ObserverNormal_Y, 3, 2, 1, 1)
        self.ObserverNormal_Z = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_Z.setObjectName(_fromUtf8("ObserverNormal_Z"))
        self.gridLayout.addWidget(self.ObserverNormal_Z, 3, 3, 1, 1)
        self.NumericalAperture_label = QtGui.QLabel(self.layoutWidget)
        self.NumericalAperture_label.setObjectName(_fromUtf8("NumericalAperture_label"))
        self.gridLayout.addWidget(self.NumericalAperture_label, 4, 0, 1, 1)
        self.label = QtGui.QLabel(self.layoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 7, 0, 1, 1)
        self.comboBox_2 = QtGui.QComboBox(self.layoutWidget)
        self.comboBox_2.setObjectName(_fromUtf8("comboBox_2"))
        self.gridLayout.addWidget(self.comboBox_2, 7, 1, 1, 2)
        self.ObserverPlaneOutputFileName_label_2 = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label_2.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label_2"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label_2, 0, 0, 1, 1)
        self.ObserverPlaneOutputFileName = QtGui.QLineEdit(self.layoutWidget)
        self.ObserverPlaneOutputFileName.setObjectName(_fromUtf8("ObserverPlaneOutputFileName"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName, 5, 1, 1, 2)
        self.ObserverOrigin_Y = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_Y.setObjectName(_fromUtf8("ObserverOrigin_Y"))
        self.gridLayout.addWidget(self.ObserverOrigin_Y, 2, 2, 1, 1)
        self.ObserverPlaneOutputFileName_label_3 = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label_3.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label_3"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label_3, 6, 0, 1, 1)
        self.lineEdit_2 = QtGui.QLineEdit(self.layoutWidget)
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.gridLayout.addWidget(self.lineEdit_2, 6, 1, 1, 2)
        self.ObserverPlaneOutputFileName_label = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label, 5, 0, 1, 1)
        self.SaveIntermediateSteps = QtGui.QCheckBox(self.layoutWidget)
        self.SaveIntermediateSteps.setObjectName(_fromUtf8("SaveIntermediateSteps"))
        self.gridLayout.addWidget(self.SaveIntermediateSteps, 8, 0, 1, 2)

        self.retranslateUi(ObserverPlaneCircleDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), ObserverPlaneCircleDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), ObserverPlaneCircleDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(ObserverPlaneCircleDialog)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverDescription, self.ObserverPlaneMode)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverPlaneMode, self.ObserverOrigin_X)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverOrigin_X, self.ObserverOrigin_Y)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverOrigin_Y, self.ObserverOrigin_Z)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverOrigin_Z, self.ObserverNormal_X)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverNormal_X, self.ObserverNormal_Y)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverNormal_Y, self.ObserverNormal_Z)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverNormal_Z, self.ObserverCircleRadius)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverCircleRadius, self.ObserverPlaneOutputFileName)
        ObserverPlaneCircleDialog.setTabOrder(self.ObserverPlaneOutputFileName, self.lineEdit_2)
        ObserverPlaneCircleDialog.setTabOrder(self.lineEdit_2, self.comboBox_2)
        ObserverPlaneCircleDialog.setTabOrder(self.comboBox_2, self.SaveIntermediateSteps)
        ObserverPlaneCircleDialog.setTabOrder(self.SaveIntermediateSteps, self.buttonBox)

    def retranslateUi(self, ObserverPlaneCircleDialog):
        ObserverPlaneCircleDialog.setWindowTitle(_translate("ObserverPlaneCircleDialog", "Edit Observer Plane/Circle", None))
        self.ObserverPlaneMode.setItemText(0, _translate("ObserverPlaneCircleDialog", "sensor", None))
        self.ObserverPlaneMode.setItemText(1, _translate("ObserverPlaneCircleDialog", "detector", None))
        self.ObserverPlaneMode_label.setText(_translate("ObserverPlaneCircleDialog", "Mode", None))
        self.ObserverCircleRadius.setToolTip(_translate("ObserverPlaneCircleDialog", "Numerical Apperture", None))
        self.SourceOrigin_label.setText(_translate("ObserverPlaneCircleDialog", "Origin", None))
        self.OutputDirection_label.setText(_translate("ObserverPlaneCircleDialog", "Normal", None))
        self.NumericalAperture_label.setText(_translate("ObserverPlaneCircleDialog", "Radius", None))
        self.label.setText(_translate("ObserverPlaneCircleDialog", "Listen on Marker", None))
        self.comboBox_2.setToolTip(_translate("ObserverPlaneCircleDialog", "Observer will only consider photons with this marker set", None))
        self.ObserverPlaneOutputFileName_label_2.setText(_translate("ObserverPlaneCircleDialog", "Description", None))
        self.ObserverPlaneOutputFileName_label_3.setText(_translate("ObserverPlaneCircleDialog", "Set Marker(s)", None))
        self.lineEdit_2.setToolTip(_translate("ObserverPlaneCircleDialog", "<html><head/><body><p>Separate Markers with \';\', Example: marker1;marker2</p></body></html>", None))
        self.ObserverPlaneOutputFileName_label.setText(_translate("ObserverPlaneCircleDialog", "Save Output As", None))
        self.SaveIntermediateSteps.setText(_translate("ObserverPlaneCircleDialog", "Save Intermediate Steps", None))

