# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt4-ui/ObserverRectangle.ui'
#
# Created: Mon Aug 26 10:16:44 2013
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ObserverRectangleDialog(object):
    def setupUi(self, ObserverRectangleDialog):
        ObserverRectangleDialog.setObjectName(_fromUtf8("ObserverRectangleDialog"))
        ObserverRectangleDialog.resize(563, 450)
        self.buttonBox = QtGui.QDialogButtonBox(ObserverRectangleDialog)
        self.buttonBox.setGeometry(QtCore.QRect(210, 410, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.layoutWidget = QtGui.QWidget(ObserverRectangleDialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 541, 387))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.OutputDirection_label = QtGui.QLabel(self.layoutWidget)
        self.OutputDirection_label.setObjectName(_fromUtf8("OutputDirection_label"))
        self.gridLayout.addWidget(self.OutputDirection_label, 3, 0, 1, 1)
        self.ObserverNormal_X = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_X.setMinimum(-99.99)
        self.ObserverNormal_X.setObjectName(_fromUtf8("ObserverNormal_X"))
        self.gridLayout.addWidget(self.ObserverNormal_X, 3, 1, 1, 1)
        self.RectangleNorth_Y = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.RectangleNorth_Y.setMinimum(-99.99)
        self.RectangleNorth_Y.setObjectName(_fromUtf8("RectangleNorth_Y"))
        self.gridLayout.addWidget(self.RectangleNorth_Y, 4, 2, 1, 1)
        self.OutputDirection_label_2 = QtGui.QLabel(self.layoutWidget)
        self.OutputDirection_label_2.setObjectName(_fromUtf8("OutputDirection_label_2"))
        self.gridLayout.addWidget(self.OutputDirection_label_2, 4, 0, 1, 1)
        self.NumericalAperture_label_2 = QtGui.QLabel(self.layoutWidget)
        self.NumericalAperture_label_2.setObjectName(_fromUtf8("NumericalAperture_label_2"))
        self.gridLayout.addWidget(self.NumericalAperture_label_2, 5, 2, 1, 1)
        self.SideB = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.SideB.setMinimum(0.0)
        self.SideB.setProperty("value", 1.0)
        self.SideB.setObjectName(_fromUtf8("SideB"))
        self.gridLayout.addWidget(self.SideB, 5, 3, 1, 1)
        self.RectangleNorth_X = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.RectangleNorth_X.setMinimum(-99.99)
        self.RectangleNorth_X.setProperty("value", 1.0)
        self.RectangleNorth_X.setObjectName(_fromUtf8("RectangleNorth_X"))
        self.gridLayout.addWidget(self.RectangleNorth_X, 4, 1, 1, 1)
        self.SideA = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.SideA.setMinimum(0.0)
        self.SideA.setProperty("value", 1.0)
        self.SideA.setObjectName(_fromUtf8("SideA"))
        self.gridLayout.addWidget(self.SideA, 5, 1, 1, 1)
        self.SourceOrigin_label = QtGui.QLabel(self.layoutWidget)
        self.SourceOrigin_label.setObjectName(_fromUtf8("SourceOrigin_label"))
        self.gridLayout.addWidget(self.SourceOrigin_label, 2, 0, 1, 1)
        self.ObserverNormal_Z = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_Z.setMinimum(-99.99)
        self.ObserverNormal_Z.setProperty("value", 1.0)
        self.ObserverNormal_Z.setObjectName(_fromUtf8("ObserverNormal_Z"))
        self.gridLayout.addWidget(self.ObserverNormal_Z, 3, 3, 1, 1)
        self.ObserverPlaneMode = QtGui.QComboBox(self.layoutWidget)
        self.ObserverPlaneMode.setObjectName(_fromUtf8("ObserverPlaneMode"))
        self.ObserverPlaneMode.addItem(_fromUtf8(""))
        self.ObserverPlaneMode.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.ObserverPlaneMode, 1, 1, 1, 1)
        self.ObserverNormal_Y = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverNormal_Y.setMinimum(-99.99)
        self.ObserverNormal_Y.setObjectName(_fromUtf8("ObserverNormal_Y"))
        self.gridLayout.addWidget(self.ObserverNormal_Y, 3, 2, 1, 1)
        self.NumericalAperture_label = QtGui.QLabel(self.layoutWidget)
        self.NumericalAperture_label.setObjectName(_fromUtf8("NumericalAperture_label"))
        self.gridLayout.addWidget(self.NumericalAperture_label, 5, 0, 1, 1)
        self.ObserverPlaneOutputFileName_label = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label, 6, 0, 1, 1)
        self.label = QtGui.QLabel(self.layoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 8, 0, 1, 1)
        self.ObserverPlaneMode_label = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneMode_label.setObjectName(_fromUtf8("ObserverPlaneMode_label"))
        self.gridLayout.addWidget(self.ObserverPlaneMode_label, 1, 0, 1, 1)
        self.ObserverDescription = QtGui.QLineEdit(self.layoutWidget)
        self.ObserverDescription.setObjectName(_fromUtf8("ObserverDescription"))
        self.gridLayout.addWidget(self.ObserverDescription, 0, 1, 1, 3)
        self.ObserverOrigin_Z = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_Z.setMinimum(-99.99)
        self.ObserverOrigin_Z.setObjectName(_fromUtf8("ObserverOrigin_Z"))
        self.gridLayout.addWidget(self.ObserverOrigin_Z, 2, 3, 1, 1)
        self.ObserverOrigin_X = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_X.setMinimum(-99.99)
        self.ObserverOrigin_X.setObjectName(_fromUtf8("ObserverOrigin_X"))
        self.gridLayout.addWidget(self.ObserverOrigin_X, 2, 1, 1, 1)
        self.ListenOnMarker = QtGui.QComboBox(self.layoutWidget)
        self.ListenOnMarker.setObjectName(_fromUtf8("ListenOnMarker"))
        self.gridLayout.addWidget(self.ListenOnMarker, 8, 1, 1, 2)
        self.ObserverPlaneOutputFileName_label_2 = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label_2.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label_2"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label_2, 0, 0, 1, 1)
        self.ObserverPlaneOutputFileName = QtGui.QLineEdit(self.layoutWidget)
        self.ObserverPlaneOutputFileName.setObjectName(_fromUtf8("ObserverPlaneOutputFileName"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName, 6, 1, 1, 2)
        self.ObserverOrigin_Y = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.ObserverOrigin_Y.setMinimum(-99.99)
        self.ObserverOrigin_Y.setObjectName(_fromUtf8("ObserverOrigin_Y"))
        self.gridLayout.addWidget(self.ObserverOrigin_Y, 2, 2, 1, 1)
        self.ObserverPlaneOutputFileName_label_3 = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label_3.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label_3"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label_3, 7, 0, 1, 1)
        self.SetMarkers = QtGui.QLineEdit(self.layoutWidget)
        self.SetMarkers.setObjectName(_fromUtf8("SetMarkers"))
        self.gridLayout.addWidget(self.SetMarkers, 7, 1, 1, 2)
        self.RectangleNorth_Z = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.RectangleNorth_Z.setMinimum(-99.99)
        self.RectangleNorth_Z.setObjectName(_fromUtf8("RectangleNorth_Z"))
        self.gridLayout.addWidget(self.RectangleNorth_Z, 4, 3, 1, 1)
        self.SaveIntermediateSteps = QtGui.QCheckBox(self.layoutWidget)
        self.SaveIntermediateSteps.setObjectName(_fromUtf8("SaveIntermediateSteps"))
        self.gridLayout.addWidget(self.SaveIntermediateSteps, 9, 0, 1, 2)

        self.retranslateUi(ObserverRectangleDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), ObserverRectangleDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), ObserverRectangleDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(ObserverRectangleDialog)
        ObserverRectangleDialog.setTabOrder(self.ObserverDescription, self.ObserverPlaneMode)
        ObserverRectangleDialog.setTabOrder(self.ObserverPlaneMode, self.ObserverOrigin_X)
        ObserverRectangleDialog.setTabOrder(self.ObserverOrigin_X, self.ObserverOrigin_Y)
        ObserverRectangleDialog.setTabOrder(self.ObserverOrigin_Y, self.ObserverOrigin_Z)
        ObserverRectangleDialog.setTabOrder(self.ObserverOrigin_Z, self.ObserverNormal_X)
        ObserverRectangleDialog.setTabOrder(self.ObserverNormal_X, self.ObserverNormal_Y)
        ObserverRectangleDialog.setTabOrder(self.ObserverNormal_Y, self.ObserverNormal_Z)
        ObserverRectangleDialog.setTabOrder(self.ObserverNormal_Z, self.RectangleNorth_X)
        ObserverRectangleDialog.setTabOrder(self.RectangleNorth_X, self.RectangleNorth_Y)
        ObserverRectangleDialog.setTabOrder(self.RectangleNorth_Y, self.RectangleNorth_Z)
        ObserverRectangleDialog.setTabOrder(self.RectangleNorth_Z, self.SideA)
        ObserverRectangleDialog.setTabOrder(self.SideA, self.SideB)
        ObserverRectangleDialog.setTabOrder(self.SideB, self.ObserverPlaneOutputFileName)
        ObserverRectangleDialog.setTabOrder(self.ObserverPlaneOutputFileName, self.SetMarkers)
        ObserverRectangleDialog.setTabOrder(self.SetMarkers, self.ListenOnMarker)
        ObserverRectangleDialog.setTabOrder(self.ListenOnMarker, self.SaveIntermediateSteps)
        ObserverRectangleDialog.setTabOrder(self.SaveIntermediateSteps, self.buttonBox)

    def retranslateUi(self, ObserverRectangleDialog):
        ObserverRectangleDialog.setWindowTitle(_translate("ObserverRectangleDialog", "Edit Observer Rectangle", None))
        self.OutputDirection_label.setText(_translate("ObserverRectangleDialog", "Normal", None))
        self.OutputDirection_label_2.setText(_translate("ObserverRectangleDialog", "Rectangle North", None))
        self.NumericalAperture_label_2.setText(_translate("ObserverRectangleDialog", "Side B", None))
        self.SideB.setToolTip(_translate("ObserverRectangleDialog", "Numerical Apperture", None))
        self.SideA.setToolTip(_translate("ObserverRectangleDialog", "Numerical Apperture", None))
        self.SourceOrigin_label.setText(_translate("ObserverRectangleDialog", "Origin", None))
        self.ObserverPlaneMode.setItemText(0, _translate("ObserverRectangleDialog", "sensor", None))
        self.ObserverPlaneMode.setItemText(1, _translate("ObserverRectangleDialog", "detector", None))
        self.NumericalAperture_label.setText(_translate("ObserverRectangleDialog", "Side A", None))
        self.ObserverPlaneOutputFileName_label.setText(_translate("ObserverRectangleDialog", "Output Name", None))
        self.label.setText(_translate("ObserverRectangleDialog", "Listen on Marker", None))
        self.ObserverPlaneMode_label.setText(_translate("ObserverRectangleDialog", "Mode", None))
        self.ListenOnMarker.setToolTip(_translate("ObserverRectangleDialog", "Observer will only consider photons with this marker set", None))
        self.ObserverPlaneOutputFileName_label_2.setText(_translate("ObserverRectangleDialog", "Description", None))
        self.ObserverPlaneOutputFileName_label_3.setText(_translate("ObserverRectangleDialog", "Set Marker(s)", None))
        self.SetMarkers.setToolTip(_translate("ObserverRectangleDialog", "<html><head/><body><p>Separate Markers with \';\', Example: marker1;marker2</p></body></html>", None))
        self.SaveIntermediateSteps.setText(_translate("ObserverRectangleDialog", "Save Intermediate Steps", None))

