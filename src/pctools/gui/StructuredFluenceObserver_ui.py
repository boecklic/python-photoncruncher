# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt4-ui/StructuredFluenceObserver.ui'
#
# Created: Mon Aug 26 10:16:44 2013
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_StructuredFluenceObserverDialog(object):
    def setupUi(self, StructuredFluenceObserverDialog):
        StructuredFluenceObserverDialog.setObjectName(_fromUtf8("StructuredFluenceObserverDialog"))
        StructuredFluenceObserverDialog.resize(561, 174)
        self.buttonBox = QtGui.QDialogButtonBox(StructuredFluenceObserverDialog)
        self.buttonBox.setGeometry(QtCore.QRect(210, 140, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.layoutWidget = QtGui.QWidget(StructuredFluenceObserverDialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 541, 131))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.ObserverPlaneOutputFileName_label = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label, 2, 0, 1, 1)
        self.Description = QtGui.QLineEdit(self.layoutWidget)
        self.Description.setObjectName(_fromUtf8("Description"))
        self.gridLayout.addWidget(self.Description, 0, 1, 1, 1)
        self.InitialValue = QtGui.QDoubleSpinBox(self.layoutWidget)
        self.InitialValue.setMinimum(0.0)
        self.InitialValue.setMaximum(1000000000.0)
        self.InitialValue.setProperty("value", 0.0)
        self.InitialValue.setObjectName(_fromUtf8("InitialValue"))
        self.gridLayout.addWidget(self.InitialValue, 1, 1, 1, 1)
        self.NumericalAperture_label = QtGui.QLabel(self.layoutWidget)
        self.NumericalAperture_label.setObjectName(_fromUtf8("NumericalAperture_label"))
        self.gridLayout.addWidget(self.NumericalAperture_label, 1, 0, 1, 1)
        self.ObserverPlaneOutputFileName_label_2 = QtGui.QLabel(self.layoutWidget)
        self.ObserverPlaneOutputFileName_label_2.setObjectName(_fromUtf8("ObserverPlaneOutputFileName_label_2"))
        self.gridLayout.addWidget(self.ObserverPlaneOutputFileName_label_2, 0, 0, 1, 1)
        self.StructuredFluenceOutputFileName = QtGui.QLineEdit(self.layoutWidget)
        self.StructuredFluenceOutputFileName.setObjectName(_fromUtf8("StructuredFluenceOutputFileName"))
        self.gridLayout.addWidget(self.StructuredFluenceOutputFileName, 2, 1, 1, 1)
        self.SaveIntermediateSteps = QtGui.QCheckBox(self.layoutWidget)
        self.SaveIntermediateSteps.setObjectName(_fromUtf8("SaveIntermediateSteps"))
        self.gridLayout.addWidget(self.SaveIntermediateSteps, 3, 1, 1, 1)

        self.retranslateUi(StructuredFluenceObserverDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), StructuredFluenceObserverDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), StructuredFluenceObserverDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(StructuredFluenceObserverDialog)
        StructuredFluenceObserverDialog.setTabOrder(self.Description, self.InitialValue)
        StructuredFluenceObserverDialog.setTabOrder(self.InitialValue, self.buttonBox)

    def retranslateUi(self, StructuredFluenceObserverDialog):
        StructuredFluenceObserverDialog.setWindowTitle(_translate("StructuredFluenceObserverDialog", "Edit Structured Fluence Observer", None))
        self.ObserverPlaneOutputFileName_label.setText(_translate("StructuredFluenceObserverDialog", "Output Name", None))
        self.InitialValue.setToolTip(_translate("StructuredFluenceObserverDialog", "Numerical Apperture", None))
        self.NumericalAperture_label.setText(_translate("StructuredFluenceObserverDialog", "Initial Value", None))
        self.ObserverPlaneOutputFileName_label_2.setText(_translate("StructuredFluenceObserverDialog", "Description", None))
        self.SaveIntermediateSteps.setText(_translate("StructuredFluenceObserverDialog", "Save Intermediate Steps", None))

