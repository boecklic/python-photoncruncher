
import vtk
import os
#import photoncruncher as pc
from photoncruncher import *

# Import all the User Interfaces
try: 
	import PyQt4
	use_ui = True	
except ImportError: 
	use_ui = False
	
if use_ui:
	from photoncruncher_ui import Ui_MainWindow
	from ObserverPlaneCircle_ui import Ui_ObserverPlaneCircleDialog
	from ObserverRectangle_ui import Ui_ObserverRectangleDialog
	from StructuredFluenceObserver_ui import Ui_StructuredFluenceObserverDialog
	from GeometryDisplayOptions_ui import Ui_GeometryDisplayOptions
# import mat
#from mcs import *


## set up the signal handling mechanism to catch the Signal SIGUSR1 which 
## gracefully shuts the simulator down
sh = pcSignalHandler();


#__all__ = ["photoncruncher","mat"];
__libpath__ = os.path.realpath(__file__)
