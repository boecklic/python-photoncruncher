#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import datetime
import shutil
import socket
import sys
from collections import OrderedDict
import json

import pclib as pc
import vtk

from configobj import ConfigObj
from validate import Validator



def in_result_directory(fn):
	"""This decorator checks, if the simulator has already chdir'd into the result directory"""
	def deco(self):
		if self.in_result_directory == True:
			fn(self)
		else:
			print "ERROR: you have to be in the result directory to call the function %s" % (fn.__name__)
			sys.exit(1)

	return deco




def setter(obj_name):
	"""The setter decorator is used to facilitate the various object-set-methods. 

	The following snipped
	
		@setter('obj')
		def ObjectName(self):
			return someObject
	
	could also be written as 
		self.obj.SetObjectName(someObject)
	"""
	def wrap(fn):
		def wrapped_fn(self):
			obj = getattr(self,obj_name)
			fun = getattr(obj,"Set%s" % (fn.__name__))
			fun(fn(self))
		return wrapped_fn
	return wrap



def ErrorEventHandler(theObject, eventName, callData):
	"""
	due to the VTK Python wrappings, raising an exception here will
	only print the error, not go through the complete exceptiong handling
	trajectory - best we can do is to toggle some error flag somewhere
	and react on it later
	"""
	print "Oooops!  Error trapped (%s): %s" % (eventName,callData,)
	sys.exit(1)

ErrorEventHandler.CallDataType = "string0"
"""Set the CallDataType attribute to string0"""





def ReadVtkImageFromFile(filename):
	"""Read a vtkImageData file and return the data as vtkImageData (wrapped PyObject*)"""
	pc.PLOG(pc.INFO,'Load Geometry (vtkImageData) from file...: ' + filename)
	reader = vtk.vtkXMLImageDataReader()
	reader.AddObserver('ErrorEvent', ErrorEventHandler) # this is used to capture an error if the file doesn't exist
	reader.SetFileName(filename)
	data = reader.GetOutput();

	# note: this is nessessary to actually pass the vtkImageData to data
	reader.Update();
	return data



def PopulateMaterialDatabase(matdbase):	

	mat = ConfigObj(matdbase+'.db',configspec=matdbase+'.spec',indent_type='\t')
	val = Validator()
	result = mat.validate(val,preserve_errors=False, copy=True)

#		print mat;
#		print result;
	if not result == True:
		print "Incorrect Material config: "
		for m in result:
			if not result[m] == True:
				print "Missing or incorrect value(s) in "+m+':'
				for n in result[m]:
					if not result[m][n] == True:
						print " -> "+n+" is missing";
						sys.exit(1)
	
	db = pc.pcMaterialDatabase_GetDB();
	
	for m in mat:
		if not db.AddMaterial(\
					pc.pcMaterial(mat[m]['ID'],\
							mat[m]['name'],\
							mat[m]['mu_a'],\
							mat[m]['mu_s'],\
							mat[m]['g'],\
							mat[m]['n'])):
			print "Error adding material: " + mat[m]['name'] + \
				"already exists."
		
	return mat


class Observer(object):
	
	def __init__(self,observer_class_name,oldPath):
		self.oldPath = oldPath

		if(observer_class_name.find('%') > -1):
			obsClass = observer_class_name[0:observer_class_name.find('%')]
		else:
			obsClass = observer_class_name
		print obsClass
		try:
			self.obs = getattr(pc, obsClass)()
			self.obsClass = obsClass
		except Exception as e:
			pc.PLOG(pc.ERR,"Unknown Observer: " + obsClass)
			pc.PLOG(pc.ERR,str(e))
			sys.exit(1)
	

	def Init(self):
		try:
			pc.PLOG(pc.INFO,"  Initializing Observer {0!s}".format(self.obs.GetClassName()))
			self.obs.Init()
		except Exception as e:
			pc.PLOG(pc.ERR,"Error during initialization of observer: " + str(self.obsClass))
			pc.PLOG(pc.ERR,str(e))	
			sys.exit(1)


	def SetOption(self,option_name,option_value):
		
		if hasattr(self, option_name):
			# check if the observer class (possibly derived) has custom option setter methods
			ret = getattr(self,option_name)(option_value)
		else:
			try:	
				ret = getattr(self.obs,option_name)(option_value)
			except Exception as e:
				pc.PLOG(pc.ERR,"Could not set value " + str(option_value) + " of " + str(type(option_value)) + \
					"\n\t for option " + str(option_name) + " \n\t for observer " + str(self.obs) + \
					"\n\t Check for correct spelling and type!");
				pc.PLOG(pc.ERR,str(e))
				sys.exit(1); 

		pc.PLOG(pc.INFO,"\t " + option_name + " = " + str(option_value))


	def GetRawObs(self):
		return self.obs


	def SetVtkImageDataObject(self,option_value):
		if option_value[0] == '/':
			path = option_value
		else:
			path = os.path.join(os.getcwd(),option_value)

		try:
			pc.PLOG(pc.INFO,'Load Geometry (vtkImageData) from file...: ' + path)

			# assign the vtkimagedata object to the geometry
			self.obs.SetVtkImageDataObject(ReadVtkImageFromFile(path))
		
		except IOError:
			pc.PLOG(pc.ERR,'Geometry file <'+ path +'> not found');
			sys.exit(1);
		except Exception as e:
			pc.PLOG(pc.ERR,"Could not set value " + str(option_value) + " of " + str(type(option_value)) +\
				"\n\t for option " + option_value + " \n\t for observer " + self.obsClass + \
				"\n\t Check for correct spelling and type!");
			pc.PLOG(pc.ERR,str(e.__class__)+ ": "+str(e))
			sys.exit(1)



	def SetListenOnMarker(self,option_value):
		try:
			pc.PLOG(pc.INFO,'Observer Listens on Marker ' + option_value)
			self.obs.SetListenOnMarker(getattr(pc,option_value))
		except Exception as e:
			pc.PLOG(pc.ERR,"Could not set value " + option_value + " of " + str(type(option_value)) +\
				"\n\t for option " + option_value + " \n\t for observer " + obs + \
				"\n\t Check for correct spelling and type!");
			pc.PLOG(pc.ERR,str(e.__class__)+ ": "+str(e))
			sys.exit(1)


class RegularGridGeometry(object):

	def __init__(self,parent):
		self.geotype = 'pcRegularGridGeometry'
		self.geo = pc.pcRegularGridGeometry()
		self.parent = parent
		self.oldPath = self.parent.oldPath


	def SetOption(self,option_name,option_value):
		if hasattr(self, option_name):
			# check if the observer class (possibly derived) has custom option setter methods
			ret = getattr(self,option_name)(option_value)
		else:

			try:
				out = getattr(self.geo,option_name)(option_value);
				pc.PLOG(pc.INFO,"\t." + option_name + " = " + str(option_value))
			except Exception as e:
				pc.PLOG(pc.ERR,"Could not set value " + str(option_value) + " of " + str(type(option_value)) +\
					"\n\t for option " + option_name + " \n\t for geometry " + self.geotype + \
					"\n\t Check for correct spelling and type!");
				pc.PLOG(pc.ERR,str(e))

	def GetRawGeo(self):
		return self.geo

	def SetVtkImageDataObject(self,option_value):
		try:
			self.geometryFilePath = os.path.join(self.oldPath,option_value)
			pc.PLOG(pc.INFO,'Load Geometry (vtkImageData) from file...: ' + self.geometryFilePath)
			
			# assign the vtkimagedata object to the geometry
			out = self.geo.SetVtkImageDataObject(ReadVtkImageFromFile(self.geometryFilePath));

			# copy the geometry file for logging purposes					
			pc.PLOG(pc.INFO,'create backup-copy of geometry file <'+ self.geometryFilePath +'>');
			shutil.copy(self.geometryFilePath, '.');
			
		
		except IOError:
			pc.PLOG(pc.ERR,'Geometry file <'+ self.geometryFilePath+'> not found');
			sys.exit(1);
		except Exception as e:
			pc.PLOG(pc.ERR,"Could not set value " + option_value + " of " + option_value +\
				"\n\t for option SetVtkImageDataObject \n\t for geometry " + self.geotype + \
				"\n\t Check for correct spelling and type!");
			pc.PLOG(pc.ERR,str(e.__class__)+ ": "+str(e))
			sys.exit(1)


class HomogeneousRegularGridGeometry(RegularGridGeometry):
	"""Declare derived dummy class for pcHomogeneousRegularGridGeometry"""
	def __init__(self,parent):
		self.geotype = 'pcHomogeneousRegularGridGeometry'
		self.geo = pc.pcHomogeneousRegularGridGeometry()
		self.parent = parent
		self.oldPath = self.parent.oldPath

	def mu_a(self,option_value):
		self.mu_a = float(option_value)

	def mu_s(self,option_value):
		self.mu_s = float(option_value)

	def g(self,option_value):
		self.g = float(option_value)

	def n(self,option_value):
		self.n = float(option_value)
		mat = pc.pcMaterial(0,"Generic Material",self.mu_a,self.mu_s,self.g,self.n)
		self.geo.SetMaterial(mat)

	def SetVtkImageDataObject(self,option_value):
		"""For the homo regular grid there's nothing to do in terms of setting the vtk image data object"""
		pc.PLOG(pc.INFO,'ignoring the option SetVtkImageDataObject for HomogeneousRegularGridGeometry')
		pass

class HomogeneousBlockGeometry(RegularGridGeometry):
	def __init__(self,parent):
		self.geo = pc.pcHomogeneousBlockGeometry()
		self.geotype = 'pcHomogeneousBlockGeometry'
		self.parent = parent
		self.oldPath = self.parent.oldPath

		# we need to have a section conf[pc.MCSIMULATOR]['Geometry']["HomogeneousBlockMaterial"]
		# where the homogeneous material of the block is defined

def GeometryFactory(geometry_class_name):
	if geometry_class_name == 'pcRegularGridGeometry':
		print RegularGridGeometry
		return RegularGridGeometry
	elif geometry_class_name == 'pcHomogeneousRegularGridGeometry':
		return HomogeneousRegularGridGeometry
	elif geometry_class_name == 'pcHomogeneousBlockGeometry':
		return HomogeneousBlockGeometry
	else:
		pc.PLOG(pc.ERR,'unknown geometry type')
		sys.exit(1)


class MonteCarloSimulator(object):


	def __init__(self,conf,**kwargs):
		#===========================================================#
		# First things first, start a new mpi handler if not provided
		#===========================================================#
		if not 'mpih' in kwargs:
			self.mpih = pc.pcMPIHandler(len(sys.argv),sys.argv)
		else:
			self.mpih = kwargs['mpih']

		# set up the logger
		pc.PLOG(pc.INFO,'Setup logger...');
		pc.SetLogLevel(conf["DebugLevel"]);
	

		#===========================================================#
		# Create and initialize the main simulator
		#===========================================================#
		pc.PLOG(pc.INFO,'\n\n====================================================================')
		self.simulator = pc.pcMonteCarloSimulator(self.mpih);
	
		# since the config Object used doesn't support long, we read number of photons as string
		#    and convert this string to a long
		self.simulator.SetNumberOfPhotons(long(float(conf["MCSimulator"]["NumberOfPhotons"])))


		# do some run-time statistics
		self.timer = pc.pcTimer()

		# save the config
		self.conf = conf

		# save the current working path (before changing to the result directory)
		self.oldPath = os.getcwd();
		



		#===========================================================#
		# Create the Geometry Container
		#===========================================================#
		self.container = pc.pcGeometryContainer();
		pc.PLOG(pc.INFO,'Creating Geometry Container...')
		self.simulator.SetGeometryContainer(self.container);

		self.in_result_directory = False

		self.timer.Start()
		pc.PLOG(pc.INFO,'Start Timer: ' + str(self.timer))


		self.callList = OrderedDict()
		self.callList['EnableConsoleLogging'] = True
		self.callList['SetupResultDirectory'] = True
		self.callList['CreateResultDirectory'] = True
		self.callList['OpenLogFile'] = True
		self.callList['CreateMaterialDatabase'] = True
		self.callList['RandomNumberGenerator'] = True
		self.callList['Collector'] = True
		self.callList['GeometryObject'] = True
		self.callList['Source'] = True
		self.callList['Method'] = True
		self.callList['Observers'] = True
		self.callList['SaveSimulationEnvironment'] = True
		self.callList['RunSimulator'] = True
		self.callList['CleanUp'] = True
		

	def Exec(self):
		# print self.callList
		for step in self.callList.iteritems():
			if step[1]:
				try:
					ret = getattr(self,step[0])()
					pc.PLOG(pc.INFO,'')
					pc.PLOG(pc.INFO,'================================================================');
					pc.PLOG(pc.INFO,'=>\t {}'.format(step[0]))
					pc.PLOG(pc.INFO,'================================================================\n');
				except Exception as e:
					pc.PLOG(pc.ERR,"Error during execution of step {0} in MonteCarloSimulator: {1.__class__!s} : {1!s}".format(step[0],e))
					# pc.PLOG(pc.ERR,u'Error during execution of step {} in MonteCarloSimulator\n'.format(step))
					sys.exit(1)


	def EnableConsoleLogging(self):
		pc.EnableConsoleLogging()


	def SetupResultDirectory(self):
		#===========================================================#
		# Create the result directory, create logfile
		#===========================================================#
		
		resDir = './'
		try:
			res = self.conf['ResultDir'];
			if(res == 'default'):
				raise Exception;
			
			if res[0] == '/':
				# An absolute path is provided
				self.resultDir = res
			else:

				resDir = resDir + res; 
				self.resultDir = os.getcwd() + "/" + resDir		
		except Exception as e:
			self.resultDir = os.getcwd() + 'MCS__'+time.strftime("%a__%d-%m-%Y__%H-%M-%S", time.localtime());
		
		

		
	def CreateResultDirectory(self):
		ok = pc.CreateResultDirectory(self.resultDir,'o');
		if(ok > 0): # error occured during creation of result directory
			pc.PLOG(pc.ERR,"oops: error when trying to create the result directory " + self.resultDir)
			del self.mpih;
			sys.exit(1)

		# change to the new directory
		os.chdir(self.resultDir);
		self.in_result_directory = True


	@in_result_directory
	def SaveSimulationEnvironment(self):
		# save the configuration to outfile
		self.conf.filename = 'config'
		if(self.mpih.GetRank() == pc.MPIMASTER):
			self.conf.write();

		# save the material database used
		# pc.PLOG(pc.INFO,"Material Database: " + self.matdb.ToString());
		self.matconf.filename = 'materials.db'
		if(self.mpih.GetRank() == pc.MPIMASTER):
			self.matconf.write();
		


	@in_result_directory	
	def OpenLogFile(self):
		# create the log file
		if "LogFileName" in self.conf:
			lfn = self.conf["LogFileName"];
		else:
			lfn = 'log';

		try: 
			pc.PLOG(pc.INFO,'opening log file... ' + lfn)
			pc.OpenLogFile(lfn);
		except Exception as e:
			pc.PLOG(pc.ERR,'Error when Opening Logfile:')
			pc.PLOG(pc.ERR,str(e))
			sys.exit(1)		


	def CreateMaterialDatabase(self):
		#===========================================================#
		# Create Material Database
		#===========================================================#
		# self.matdb = pc.mat.pcMaterialDatabase_GetDB();
		# self.matdb.Reset();
		if self.conf['MaterialDB'][0] == '/':
			matdbPath = self.conf['MaterialDB']
		else:
			matdbPath = os.path.join(self.oldPath,self.conf['MaterialDB'])
		if matdbPath[-3:] == '.db':
			# The actual file with ending .db was chosen
			matdbPath = matdbPath[:-3]
		self.matconf = PopulateMaterialDatabase(matdbPath);


	@setter('simulator')
	def RandomNumberGenerator(self):
		#===========================================================#
		# Create the Random Number Generator
		# Set the seed to the rank number of the mpi process
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup Random Number Generator');
		try:
			self.gslRNG = pc.pcGSLRandomNumberGenerator(conf['RNG']['GslRngType'],self.mpih.GetRank());
		except:
			pc.PLOG(pc.WARN,'GslRngType or GslRngSeed keys not defined!')
			self.gslRNG = pc.pcGSLRandomNumberGenerator('mt19937',0);

		return self.gslRNG;


	
	@setter('container')
	def Collector(self):
		#===========================================================#
		# Define a photon Collector
		#===========================================================#
		pc.PLOG(pc.INFO,'\n\n====================================================================')
		self.collector = pc.pcPhotonCollector();
		return self.collector
		#collector = pc.pcSavePhotonsToFileCollector('photonbin.txt')
		# self.container.SetCollector(self.collector);
		# pc.PLOG(pc.INFO,'Attach photon collector <'+collector.GetClassName()+'> to container...')




	@in_result_directory
	@setter('container')
	def GeometryObject(self):
		#===========================================================#
		# Load geometry from file and create geometry object
		#===========================================================#
		
		pc.PLOG(pc.INFO,'\n\n====================================================================')
		pc.PLOG(pc.INFO,'Creating Geometry Object...')
	
		
		# check if we have a Geometry section in the config file
		if not 'Geometry' in self.conf[pc.MCSIMULATOR]:
			pc.PLOG(pc.ERR,'No [[Geometry]] section found!')
			sys.exit(1)
			
		
		# try to create the geometry object

		try:
			geotype = self.conf[pc.MCSIMULATOR]['Geometry']['type']
			# self.geoObject = getattr(pc, geotype)()			
			self.geo = GeometryFactory(geotype)(self)
			# print geotype
			# print GeometryFactory(geotype)
		except KeyError as e:
			pc.PLOG(pc.ERR,'Geometry type not specified! (use e.g. type = "pcVtkImageDataGeometry")')
			sys.exit(1)
		except Exception as e:
			pc.PLOG(pc.ERR,'Error when allocating geometry object: ' + geotype)
			pc.PLOG(pc.ERR,str(e))
			sys.exit(1)		


		
		# set options for the geometry object
		geooptions = self.conf[pc.MCSIMULATOR]['Geometry'][geotype]
		for option_name,option_value in geooptions.iteritems():
			self.geo.SetOption(option_name,option_value)


		# special case for the homogeneous block, as the material is provided by the Batch-Script
		# ----------------------------------------------------
		# ==> This has to go to derived class!!!!!!!1
		# if geotype == 'pcHomogeneousBlockGeometry' or geotype == 'pcHomogeneousRegularGridGeometry':
		# 	try:
		# 	  	homomat = options['homogeneousMaterial'];
		# 		pc.PLOG(pc.INFO,'Using a block of homogeneous material:' + str(homomat));
		# 		geoObject.SetMaterial(homomat);
		# 	except Exception as e:
		# 		pc.PLOG(pc.ERR,'homogeneousMaterial not defined in options')
		# 		pc.PLOG(pc.ERR,str(e))
		# 		sys.exit(1);
		# ----------------------------------------------------


		# check if the provided options are sufficient to correctly initialize the geometry object
		if not self.geo.GetRawGeo().IsCorrectlyInitialized():
			pc.PLOG(pc.ERR,'The geometry was not initialized correctly. Check your config')
	#		pc.PLOG(pc.ERR,str(e))
			sys.exit(1);


		# attach the geometry Object to the container
		return self.geo.GetRawGeo();
		

	@setter('container')
	def Source(self):
		
		#===========================================================#
		# Define a photon source
		#===========================================================#
		pc.PLOG(pc.INFO,'\n\n====================================================================')
		pc.PLOG(pc.INFO,'Creating Source Object ...')
		
		#########			
		if not 'Sources' in self.conf[pc.MCSIMULATOR]:
			pc.PLOG(pc.ERR,"Couldn't find any type of source!!")	
			sys.exit(1)


		try:
			sourcetype = self.conf[pc.MCSIMULATOR]['Sources']['type']
			self.source = getattr(pc, sourcetype)()			
		except KeyError as e:
			pc.PLOG(pc.ERR,'Source type not specified! (use e.g. type = "pcPhotonPointSource")')
			sys.exit(1)
		except Exception as e:
			pc.PLOG(pc.ERR,'Error when allocating source object:')
			pc.PLOG(pc.ERR,str(e))
			sys.exit(1)
			
		sourceoptions = self.conf[pc.MCSIMULATOR]['Sources'][sourcetype]
		for opt in sourceoptions:
			try:
				out = getattr(self.source,opt)(sourceoptions[opt]);
				pc.PLOG(pc.INFO,"\t." + opt + " = " + str(sourceoptions[opt]))
			except Exception as e:
				pc.PLOG(pc.ERR,"Could not set value " + str(sourceoptions[opt]) + " of " + str(type(sourceoptions[opt])) +\
					"\n\t for option " + opt + " \n\t for source " + sourcetype + \
					"\n\t Check for correct spelling and type!");
				pc.PLOG(pc.ERR,str(e))
			
		return self.source;
	

	@setter('simulator')
	def Method(self):
		#===========================================================#
		# Define and setup a simulation method
		#===========================================================#
		pc.PLOG(pc.INFO,'\n\n====================================================================')
		methname = self.conf[pc.MCSIMULATOR][pc.MONTECARLOMETHOD]
		methoptions = self.conf[pc.MCSIMULATOR][pc.MCMETHOD][methname]
		try:
			self.method = getattr(pc, methname)()
			pc.PLOG(pc.INFO,"Creating MonteCarloMethod " + methname)
		except:
			pc.PLOG(pc.ERR,'Unknown Monte-Carlo Method {}'.format(methname))
			sys.exit(1)
		
		pc.PLOG(pc.INFO,'\t.setting GeometryObject')
		print dir(self.method)
		self.method.SetGeometryObject(self.geo.GetRawGeo());
		
		
		for opt in methoptions:
			try:
				out = getattr(self.method,opt)(methoptions[opt])
				pc.PLOG(pc.INFO,"\t." + opt + " = " + str(methoptions[opt]))
			except Exception as e:
				pc.PLOG(pc.ERR,"Could not set value " + str(methoptions[opt]) + " of " + str(type(methoptions[opt])) +\
					"\n\t for option " + opt + " \n\t for method " + methname + \
					"\n\t Check for correct spelling and type!");
				pc.PLOG(pc.ERR,str(e))

				sys.exit(1);
		
		



		return self.method;


	@in_result_directory
	def Observers(self):
		#===========================================================#
		# we want to observe some things:
		#===========================================================#
		self.observers = {};
		for observer_type in self.conf["Observers"]:
			self.observers[observer_type] = [];			
			observer_class_list = self.conf["Observers"][observer_type]
			for observer_name in observer_class_list:

				# create the observer object (wrapped as python Observer-object)
				obs = Observer(observer_name,self.oldPath)
				print obs.GetRawObs()
				self.simulator.AttachObserver(obs.GetRawObs())
				self.observers[observer_type].append(obs.GetRawObs())

				# setting all options for this observer
				for (option_name,option_value) in observer_class_list[observer_name].iteritems():
					obs.SetOption(option_name,option_value)
				
				obs.Init()


	# pc.PLOG(pc.INFO,'\n\n====================================================================')
	# ## special case for ObserverPlanes, as we globally have to set the MatlabResultsCollector
	# matResultsCollector = pc.pcMatlabResultsCollector("MatlabResultsCollector.mat")
# 			--------------------------------------------------------
# 			## special case for ObserverPlanes, as we globally have to set the MatlabResultsCollector
# 			if o.GetClassName() == "pcObserverPlane" or\
# 				o.GetClassName() == "pcObserverRectangle":
# 				o.SetMatlabResultsCollector(matResultsCollector)
# # 			elif o.GetClassName() == "pcStructuredIntensityObserver":
# # #				o.AddMaterialData(geoObject.GetMaterialMap())
# 			--------------------------------------------------------


	

	@in_result_directory
	def RunSimulator(self):
		#===========================================================#
		# go!!
		#===========================================================#
		
		pc.PLOG(pc.INFO,'')
		pc.PLOG(pc.INFO,'================================================================');
		pc.PLOG(pc.INFO,'=>\t START SIMULATION')
		pc.PLOG(pc.INFO,'================================================================\n');
		
		try:
			self.simulator.run();
		except Exception as e:
			pc.PLOG(pc.ERR,"Runtime Error, no results will be printed!")
			pc.PLOG(pc.ERR,str(e))
			os.chdir(self.oldPath);
			return 0;
		



	def CleanUp(self):
		#===========================================================#
		# Clean-up 
		#===========================================================#
		
			
		pc.PLOG(pc.INFO,'Stop Timer. This simulation took ' + str(self.timer) + ' seconds.')
		self.timer.Stop()
		
		
		respath = os.getcwd()
		ressize = sum([os.path.getsize(f) for f in os.listdir(respath) if os.path.isfile(f)]);

		pc.PLOG(pc.INFO,'The simulation finished at ' + time.strftime("%a__%d-%m-%Y__%H-%M-%S", time.localtime()));
		pc.PLOG(pc.INFO,'The results are saved in ' + respath)
		pc.PLOG(pc.INFO,'The size of the result dir is ('+ str(ressize/1024/1024.0) +' MB)');

		# change to the old path
		os.chdir(self.oldPath);	

	


   
