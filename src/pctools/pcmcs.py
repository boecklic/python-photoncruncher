#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
.. module:: pctools
   :platform: Unix, Windows
   :synopsis: A useful module indeed.

.. moduleauthor:: Christoph Böcklin <christoph.boecklin@gmail.com>


"""


import os
import time
# import datetime
import shutil
# import socket
import sys
from collections import OrderedDict
# import json

import pclib as pc
import vtk

from configobj import ConfigObj
from validate import Validator

from pkg_resources import resource_filename


from abc import ABCMeta, abstractmethod

from json import JSONEncoder
import json


from pclib import MATERIAL_MARKER

#Config Specification file for PhotonCruncher

# Set debug output level
# =========================
# choose from:
# ERROR, WARNING, INFO, DEBUG, DEBUG1, DEBUG2, DEBUG3, DEBUG4
#
# DEBUG_LEVEL = 'INFO'


# mpih1 = pc.pcMPIHandler(0,[])
# 

def any2bool(v):
	if type(v) == bool:
		return v
	else:
		return v.lower() in ("yes", "true", "t", "1")




class ParamNotListError(Exception):
	pass

class ListTooShortError(Exception):
	pass

class ListTooLongError(Exception):
	pass


class ConversionFailedError(Exception):

	def __init__(self,msg):
		self.msg = msg

	def __str__(self):
		return self.msg

class ObjectCreationError(Exception):

	def __init__(self,msg):
		self.msg = msg

	def __str__(self):
		return self.msg

class ParameterSettingError(Exception):

	def __init__(self,msg):
		self.msg = msg

	def __str__(self):
		return self.msg




def defaultConverter(arg,**kwargs):
	return arg



def floatConverter(arg,**kwargs):
	return float(arg)

def intConverter(arg,**kwargs):
	return int(arg)

def floatListConverter(arg,**kwargs):
	if not type(arg) == type([]):
		raise ParamNotListError

	if 'min_length' in kwargs:
		if len(arg) < kwargs['min_length']:
			raise ListTooShortError
	if 'max_length' in kwargs:
		if len(arg) > kwargs['max_length']:
			raise ListTooLongError
	
	return map(lambda x: float(x),arg)



def intListConverter(arg,**kwargs):
	if not type(arg) == type([]):
		raise ParamNotListError

	if 'min_length' in kwargs:
		if len(arg) < kwargs['min_length']:
			raise ListTooShortError
	if 'max_length' in kwargs:
		if len(arg) > kwargs['max_length']:
			raise ListTooLongError
	
	return map(lambda x: int(x),arg)



def stringListConverter(arg,**kwargs):
	if not type(arg) == type([]):
		raise ParamNotListError

	if 'min_length' in kwargs:
		if len(arg) < kwargs['min_length']:
			raise ListTooShortError
	if 'max_length' in kwargs:
		if len(arg) > kwargs['max_length']:
			raise ListTooLongError

	return map(lambda x: str(x),arg)


def objectFromConfigurableConverter(arg,**kwargs):
	if not isinstance(arg,Configurable):
		raise TypeError('object must be inherited from Configurable')
	# print 'obj:'
	# print arg._obj
	return arg._obj


def pathToVtkImageConverter(filename,**kwargs):
	"""Read a vtkImageData file and return the data as vtkImageData (wrapped PyObject*)"""
	pc.PLOG(pc.INFO,'Load Geometry (vtkImageData) from file...: ' + filename)
	reader = vtk.vtkXMLImageDataReader()
	reader.AddObserver('ErrorEvent', ErrorEventHandler) # this is used to capture an error if the file doesn't exist
	reader.SetFileName(filename)
	data = reader.GetOutput()

	# note: this is nessessary to actually pass the vtkImageData to data
	reader.Update()
	# print data
	return data



def booleanConverter(arg,**kwargs):
	return any2bool(arg)


class Param(object):
	def __init__(self,varname,defaultvalue,**kwargs):
		self.varname = varname
		self.value = defaultvalue
		self.converter = defaultConverter
		self.kwargs = kwargs
		self.optional = kwargs.get('optional',False)

	def update(self,value):
		self.value = value
		# print 'updating parameter {} with {}'.format(self.varname,value)
		
		# This is only executed if the param is set in the configuration
		# hence it is not optional
		self.optional = False

	

	def set(self,configurable):
		if not isinstance(configurable,Configurable):
			print 'you should pass an configurable object to set'
		
		if not self.optional:

			configurable._makeParam(
				self.varname, 
				self.value, 
				self.converter, 
				**self.kwargs)

class ObjectParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(ObjectParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = objectFromConfigurableConverter

class FloatParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(FloatParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = floatConverter

class FloatListParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(FloatListParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = floatListConverter

class IntParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(IntParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = intConverter


class IntListParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(IntListParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = intListConverter


class StringListParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(StringListParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = stringListConverter


class VtkImageParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(VtkImageParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = pathToVtkImageConverter

		# copy the geometry file for logging purposes					
		# pc.PLOG(pc.INFO,'create backup-copy of geometry file <'+ self.geometryFilePath +'>');
		# shutil.copy(self.geometryFilePath, '.');

class BooleanParam(Param):
	def __init__(self,varname,defaultvalue,**kwargs):
		super(BooleanParam,self).__init__(varname,defaultvalue,**kwargs)
		self.converter = booleanConverter


class AggregatedParam(Param):
	def __init__(self,varname,paramlist,**kwargs):
		if type(paramlist) != type([]):
			raise TypeError
		super(AggregatedParam,self).__init__(varname,paramlist,**kwargs)
		self.paramlist = paramlist

	def update(self,updatelist):
		if len(updatelist) != len(self.paramlist):
			raise ListTooShortError
		print self.paramlist
		for param,updateval in zip(self.paramlist,updatelist):
			param.update(updateval)
		print self.paramlist

		self.optional = False

	def set(self,configurable):
		if not isinstance(configurable,Configurable):
			print 'you should pass an configurable object to set'
		
		print self.paramlist
		# self.value = map(lambda x: x.converter(x.value),self.paramlist)
		print self.value
		self.kwargs['paramlist'] = True

		if not self.optional:

			configurable._makeParam(
				self.varname, 
				self.value, 
				self.converter, 
				**self.kwargs)
		else:
			print '{} is optional, skipping'.format(self.varname)

# from collections import OrderedDict
class Configurable(object):

	def __init__(self,*args,**kwargs):
		"""
		This method actually tries to create an object from photoncruncher with the given 
		class name and corresponding parameters
		"""

		self._kwargs = kwargs

		self._name = self.__class__.__name__

		# self._paramList = {}
		self._paramList = OrderedDict()

		# Add all possible Parameters the the dict _paramList
		self._addParams()

		

		# Check if the passed parameters are valid
		for key,val in self._kwargs.iteritems():
			if key in self._paramList:
				# Overwrite default value
				# print 'updating {} with {}'.format(key,val)
				self._paramList[key].update(val)
			else:
				raise ParameterSettingError('"{}" doesn\'t seem to be a valid parameter for {}'.format(key,self._name))

		# Check the parameter list, e.g. pop some params from the list if anotherone is set!
		self._checkParams()

		# Create the object
		try:
			if self._name == 'pcMPIHandler':
				self._obj = getattr(pc,self._name)(len(sys.argv),sys.argv)
			else:
				self._obj = getattr(pc,self._name)()
			pc.PLOG(pc.INFO,'created object {}'.format(self._name))
		except Exception as e:
			raise ObjectCreationError('creation of object {} failed: {}'.format(self._name,e))
		

	def setup(self):
		# Set the parameters later
		for key,obj in self._paramList.iteritems():
			print 'setting key: {}, obj: {}'.format(key,obj)
			if isinstance(obj.value,Configurable):
				obj.value.setup()
			obj.set(self)




	def _addParams(self):
		# raise NotImplementedError	
		pass


	def _addParam(self,param):
		if not isinstance(param,Param):
			print 'I dont know how to add {}, I need a param instance'.format(param)
			sys.exit(1)
		self._paramList[param.varname] = param

	def _checkParams(self):
		pass

	def _makeParam(self,varname,defaultvalue,converter=None,**kwargs):
		# Try to convert the parameters to necessary types
		# 
		if not converter:
			converter = self.defaultConverter
		
		try:
			# val = converter(self._kwargs.get(varname,defaultvalue),**kwargs)
			val = converter(defaultvalue,**kwargs)

		except Exception as e:
			# print dir(converter.__func__.__name__)
			print converter
			raise ConversionFailedError('Conversion of {} with {} failed: {}'.format(defaultvalue,converter.__func__.__name__,e.__class__.__name__))
			# sys.exit(1)

		# print '_makeParam:val'
		# print val
		# Try to actually set the values
		paramlist = kwargs.get('paramlist',False)
		print kwargs
		try:
			if paramlist:
				print 'paramlist'
				print defaultvalue
				print val
				vals = map(lambda x: x.converter(x.value),val)
				print vals
				getattr(self._obj,'Set{}'.format(varname))(*vals)	
			else:
				getattr(self._obj,'Set{}'.format(varname))(val)
			
			try:
				pc.PLOG(pc.INFO,'     - setting {}:{} = {} ({})'.format(self.__class__.__name__,
																	varname,
																	val,
																	type(val)
																))
			except:
				pc.PLOG(pc.INFO,'     - setting {}:{}'.format(self.__class__.__name__,
																	varname
																))
		except Exception as e:
			raise ParameterSettingError('Could not set param {} (of type {}) for {}: {}'.format(varname,type(val),self._name,e))
			



	def to_json(self):
		return { '__type__': self._name, 'kwargs': self._kwargs }



class pcMPIHandler(Configurable):
	def NotImplemented(self):
		return False


	def __init__(self,*args,**kwargs):
		super(pcMPIHandler,self).__init__(*args,**kwargs)


	def _addParams(self):
		pass


	def GetRank(self):
		return self._obj.GetRank()

MPIH = pcMPIHandler(len(sys.argv),sys.argv)


# --------------------------------------------------------
# METHODS


class MonteCarloMethod(Configurable):
	__metaclass__ = ABCMeta

	@abstractmethod
	def NotImplemented(self):
		pass




	def __init__(self,*args,**kwargs):
		super(MonteCarloMethod,self).__init__(*args,**kwargs)



class pcSinglePhotonFixedStepsize(MonteCarloMethod):
	"""
	A single photon (with weight 1) travels a fixed stepsize. It is checked
	if the photon is scattered, or absorbed. If it is scattered, it continues
	in the new direction, again with the same fixed stepsize. If it is absorbed, 
	the photon dies and a new photon is lauched. 
	This is the simplest approach, but very inefficient.
	
	:param Stepsize: the fixed stepsize (default: 0.001 [mm] )
	"""

	def NotImplemented(self):
		return False	

	def __init__(self,*args,**kwargs):
		super(pcSinglePhotonFixedStepsize,self).__init__(*args,**kwargs)	
	
	def _addParams(self):	
		super(pcSinglePhotonFixedStepsize,self)._addParams()
		self._addParam(FloatParam('Stepsize','0.001'))


class pcSinglePhotonVariableStepsize(MonteCarloMethod):
	"""
	This method currently doesn't need any parameters to be configured
	"""

	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcSinglePhotonVariableStepsize,self).__init__(*args,**kwargs)


class pcPhotonPacketVariableStepsize(MonteCarloMethod):

	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcPhotonPacketVariableStepsize,self).__init__(*args,**kwargs)

	def _addParams(self):
		super(pcPhotonPacketVariableStepsize,self)._addParams()	
		self._addParam(FloatParam('RouletteChance','10'))
		self._addParam(FloatParam('WeightThreshold','0.1'))


class pcPhotonPacketVariableStepsizeStepping(MonteCarloMethod):

	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcPhotonPacketVariableStepsizeStepping,self).__init__(*args,**kwargs)

	def _addParams(self):	
		super(pcPhotonPacketVariableStepsizeStepping,self)._addParams()
		self._addParam(FloatParam('RouletteChance','10'))
		self._addParam(FloatParam('WeightThreshold','0.1'))

class pcPhotonPacketVariableStepsizeStepping2(MonteCarloMethod):
	"""
	A photon packet is launched from the source. Depending on the current material,
	a new (variable) stepsize is calculated. The photon packet has an initial weight 
	usually higher than 1 (typically 100 or 1000). The photon packet is propagated
	in the given direction till it crosses a voxel boundary. The voxel intersection point is calculated
	and the photon is in the next voxel. If the remaining stepsize is larger than the voxel,
	the packet is propagated again to the voxel intersection point, otherwise to the end-point
	within the voxel. 

	This method is only useful if you use a regular grid geometry. If you want to use the 
	pcStructuredFluenceObserver, you have to use this method.


	:param RouletteChance: Determines the probability that a photon packet survives once its weights falls below WeightThreshold. If the RouletteChance
							is e.g. 10, 1 in 10 packages will survive. (default: 10)
	:param WeightThreshold: If the weight of the photon packet falls below this threshold, it has once chance to survive (depending on RouletteChance), otherwise 
							 it is killed.
	"""
	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcPhotonPacketVariableStepsizeStepping2,self).__init__(*args,**kwargs)

	def _addParams(self):
		super(pcPhotonPacketVariableStepsizeStepping2,self)._addParams()	
		self._addParam(FloatParam('RouletteChance','10'))
		self._addParam(FloatParam('WeightThreshold','0.1'))


# --------------------------------------------------------
# SOURCES

class pcIsotropicPhotonPointSource(Configurable):
	"""
	A point source which launches photon packets uniformly in all 3 directions.

	:param Origin: 	The origin of the point source (default: [0,0,0])
	:type Origin: list
	:param InitialPhotonWeight: The initial weight of a photon(packet) (default: 100)
	:type InitialPhotonWeight: float
	"""

	def __init__(self,*args,**kwargs):
		super(pcIsotropicPhotonPointSource,self).__init__(*args,**kwargs)


	def _addParams(self):
		self._addParam(FloatListParam('Origin',[0.0,0.0,0.0],min_length=3,max_length=3))
		self._addParam(FloatParam('InitialPhotonWeight',100.0))


class pcPhotonPointSource(pcIsotropicPhotonPointSource):
	"""
	A point source which launches photon packets in a given direction. A numerical apperture can be specified
	to mimick e.g. a fiber end tip.

	:param Origin: 	The origin of the point source (default: [0,0,0])
	:type Origin: list
	:param InitialPhotonWeight: The initial weight of a photon(packet) (default: 100)
	:type InitialPhotonWeight: float
	:param OutputDirection: The initial direction of photons launched from the source. (default: [1.0,0,0])
	:type OutputDirection: float list
	:param NumericalAperture: The numercal apperture of the source (default: 0.0)
	:type NumericalAperture: float
	"""

	def __init__(self,*args,**kwargs):
		super(pcPhotonPointSource,self).__init__(*args,**kwargs)

	def _addParams(self):
		super(pcPhotonPointSource,self)._addParams()
		self._addParam(FloatListParam('OutputDirection',[1.0,0.0,0.0],min_length=3,max_length=3))
		self._addParam(FloatParam('NumericalAperture',0.0))
		


class pcPhotonBeamSource(pcIsotropicPhotonPointSource):
	"""
	The pcPhotonBeamSource mimicks a gaussian beam profile at the source position. The photon are all launched in the same direction, specified by OutputDirection, hence
	it is not expected to obtain a gaussian beam profile when launched in air. The photons will simply have a gaussian profile with a waist specified by BeamRadius at the source position.

	:param OutputDirection: The initial direction of photons launched from the source. (default: [1.0,0,0])
	:type OutputDirection: float list
	:param BeamRadius: The waist (or beam radius) of the beam at the source position (default: 0.0)
	:type BeamRadius: float
	"""

	def __init__(self,*args,**kwargs):
		super(pcPhotonBeamSource,self).__init__(*args,**kwargs)


	def _addParams(self):
		super(pcPhotonBeamSource,self)._addParams()
		self._addParam(FloatListParam('OutputDirection',[1.0,0.0,0.0],min_length=3,max_length=3))
		self._addParam(FloatParam('BeamRadius',0.0))




# --------------------------------------------------------
# GEOMETRIES

class pcGeometry(Configurable):
	__metaclass__ = ABCMeta

	@abstractmethod
	def NotImplemented(self):
		pass



	def __init__(self,*args,**kwargs):
		super(pcGeometry,self).__init__(*args,**kwargs)




class pcRegularGridGeometry(pcGeometry):
	"""
	The regular grid is the general form of a geometry. It consists basically of a 3D image, where every voxel is assigned an index corresponding to 
	a material in the material database. This can be used to define certain voxels as air, hence creating arbitrary (voxelized) geometries this way.

	:param VtkImageDataObject: path to a vtk (or vti) file containing the 3d image data
	:type VtkImageDataObject: string (path)
	"""

	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcRegularGridGeometry,self).__init__(*args,**kwargs)


	def _addParams(self):
		super(pcRegularGridGeometry,self)._addParams()
		self._addParam(VtkImageParam('VtkImageDataObject','',optional=True))

		self._addParam(IntListParam('Dimensions',[10,10,10],min_length=3,max_length=3))
		self._addParam(FloatListParam('Spacing',[1,1,1],min_length=3,max_length=3))
		self._addParam(FloatListParam('Origin',[0,0,0],min_length=3,max_length=3))

		self._addParam(AggregatedParam('DefineMaterialQuboid',
					[FloatListParam('Point1',[0,0,0]),
					FloatListParam('Point2',[1,1,1]),
					IntParam('MaterialID',0)],optional=True))
		# 
	def _checkParams(self):
		if self._paramList['VtkImageDataObject'].value:
			# If the parameter vtkimagedataobject is set, pop the other (default) parameters to prevent interference!
			pc.PLOG(pc.INFO,'\tremoving parameters dimension, spacing and origin from pcRegularGridGeometry')
			self._paramList.pop('Dimensions')
			self._paramList.pop('Spacing')
			self._paramList.pop('Origin')	
		

class pcHomogeneousRegularGridGeometry(pcGeometry):
	"""
	The pcHomogeneousRegularGridGeometry is simpler in that it consists of a regular grid which contains only one material. The material can directly be defined in 
	the configuration (this makes material sweeps easier).

	:param Dimensions: Dimensions of the geometry grid, list of three int values (takes only effect, if no VtkImageDataObject has been specified) (default: [10,10,10])
	:type Dimensions: int list
	:param Spacing: Spacing of the grid (default: [1,1,1])
	:type Spacing: float list
	:param Origin: Origin of the point (0,0,0) of the geometry (default: [0,0,0])
	:param Material: The material of the homogeneous geometry
	:type Material: pcMaterial (type --help pcMaterial to get more information on this)
	"""	
	def NotImplemented(self):
		return False


	def __init__(self,*args,**kwargs):
		super(pcHomogeneousRegularGridGeometry,self).__init__(*args,**kwargs)


	def _addParams(self):
		super(pcHomogeneousRegularGridGeometry,self)._addParams()
		self._addParam(IntListParam('Dimensions',[10,10,10],min_length=3,max_length=3))
		self._addParam(FloatListParam('Spacing',[1,1,1],min_length=3,max_length=3))
		self._addParam(FloatListParam('Origin',[0,0,0],min_length=3,max_length=3))



		self._addParam(ObjectParam('Material',None))



class pcMaterial(Configurable):
	"""
	Define a material

	:param Mu_a: The absorption coefficient of the material (default: 0.01)
	:type Mu_a: float
	:param Mu_s: The scattering coefficient of the material (default: 1.0)
	:type Mu_s: float
	:param G: The scattering anistropy (Value between -1 and 1) (default: 0.0)
	:type G: float
	:param RefractiveIndex: The RefractiveIndex index of the material (value >= 1.0) (default: 1.0)
	:type RefractiveIndex: float
	"""
	def NotImplemented(self):
		return False


	def __init__(self,*args,**kwargs):
		super(pcMaterial,self).__init__(*args,**kwargs)


	def _addParams(self):
		# super(pcMaterial,self)._addParams()
		self._addParam(FloatParam('Mu_a',0.01))
		self._addParam(FloatParam('Mu_s',1.0))
		self._addParam(FloatParam('G',0.0))
		self._addParam(FloatParam('RefractiveIndex',1.0))



# --------------------------------------------------------
# OBSERVERS



class pcObserver(Configurable):
	__metaclass__ = ABCMeta

	@abstractmethod
	def NotImplemented(self):
		pass

	def __init__(self,*args,**kwargs):
		super(pcObserver,self).__init__(*args,**kwargs)

	def _addParams(self):
		self._addParam(Param('OutputFileName','log'))
		self._addParam(Param('Description',''))
		self._addParam(BooleanParam('SaveIntermediateSteps',False))

class pcMaterialMarker(Configurable):
	"""
	An observer which marks a photon once it reaches a certain material. This
	can be useful to check e.g. whether a photon has reached a certain depth.
	"""
	def __init__(self,*args,**kwargs):
		super(pcMaterialMarker,self).__init__(*args,**kwargs)

	def _addParams(self):
		# [[[pcMaterialMarker]]]
		# ID=19 is white matter
		# SetMarkerMaterialId = 19
		self._addParam(IntParam('MarkerMaterialId',0))


class pcStructuredObserver(pcObserver):
	__metaclass__ = ABCMeta

	@abstractmethod
	def NotImplemented(self):
		pass
	

	def __init__(self,*args,**kwargs):
		super(pcStructuredObserver,self).__init__(*args,**kwargs)
	
	def _addParams(self):
		super(pcStructuredObserver,self)._addParams()
		self._addParam(IntListParam('Dimensions',[10,10,10],min_length=3,max_length=3))
		self._addParam(FloatListParam('Spacing',[1,1,1],min_length=3,max_length=3))
		self._addParam(FloatListParam('Origin',[1,1,1],min_length=3,max_length=3))
		



class pcStructuredFluenceObserver(pcStructuredObserver):
	"""
	Create a spatially resolved grid of the fluence in the geometry. This observer can only be used if the geometry is 
	a regular grid geometry. The grid can have the same dimensions as the geometry, but this is not required.

	:param VtkImageDataObject: A vtk file that defines the geometry and the resolution of the fluence observer
	:param Dimensions: Dimensions of the observer grid, list of three int values (takes only effect, if no VtkImageDataObject has been specified) (default: [10,10,10])
	:type Dimensions: int list
	:param Spacing: Spacing of the grid (default: [1,1,1])
	:type Spacing: float list
	:param Origin: Origin of the point (0,0,0) of the geometry (default: [0,0,0])
	"""
	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcStructuredFluenceObserver,self).__init__(*args,**kwargs)

	def _addParams(self):
		super(pcStructuredFluenceObserver,self)._addParams()
		self._addParam(FloatParam('InitDefaults',0.0))
		self._addParam(VtkImageParam('VtkImageDataObject',None,optional=True))


	def _checkParams(self):
		if self._paramList['VtkImageDataObject'].value:
			# If the parameter vtkimagedataobject is set, pop the other (default) parameters to prevent interference!
			pc.PLOG(pc.INFO,'\tremoving parameters dimension, spacing and origin from pcStructuredFluenceObserver')
			self._paramList.pop('Dimensions')
			self._paramList.pop('Spacing')
			self._paramList.pop('Origin')


class pcStructuredAbsorptionPositionObserver(pcStructuredObserver):
	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcStructuredAbsorptionPositionObserver,self).__init__(*args,**kwargs)




class pcObserverPlane(pcObserver):
	__metaclass__ = ABCMeta

	@abstractmethod
	def NotImplemented(self):
		pass



	def __init__(self,*args,**kwargs):
		super(pcObserverPlane,self).__init__(*args,**kwargs)


	def _addParams(self):
		super(pcObserverPlane,self)._addParams()
		self._addParam(Param('Mode','sensor')) #'sensor','detector'
		self._addParam(FloatListParam('Normal',[1,0,0],min_length=3,max_length=3))
		self._addParam(FloatListParam('Origin',[0,0,0],min_length=3,max_length=3))

		
		# self._addParam(StringListParam('Marker',[]))
		self._addParam(Param('ListenOnMarker',None,optional=True))


class pcObserverCircle(pcObserverPlane):
	"""
	Mimicking a circular photo-detector. If the radius is < 0, the whole plane specified by 
	'Origin' and 'Normal' acts as a detector, otherwise only the circular area limited by 'radius' 
	acts as a detector area.

	:param Origin: The center position of the rectangle (default: [0,0,0])
	:type Origin: float list
	:param Normal: The normal vector of the rectangle plane, photons are only considered by the observer, if the scalar product between their direction and the normal is pcStructuredAbsorptionPositionObserver
	:type Normal: float list
	:param Radius: if a radius > 0 is specified, only photons crossing the circle with the specified Radius are detected, otherwise the whole plane acts as observer area
	:type Radius: float
	:param Mode: can be either detector or sensor. In detector mode, the photon is killed after it has been detected. In sensor mode, it continues to live after it has been observed by the rectangle (default: 'sensor')
	:type Mode: {'sensor','detector'} 
	"""
	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcObserverCircle,self).__init__(*args,**kwargs)



	def _addParams(self):
		super(pcObserverCircle,self)._addParams()
		self._addParam(FloatParam('Radius',-1.0)) # -1 means the whole plane


class pcObserverRectangle(pcObserverPlane):
	"""
	Mimicking a rectangular photo-detector defined by the unit vectors 'Origin', 'Normal', 'RectangleNorth' and 
	the scalars 'SideA' and 'SideB'. 'SideB' is in the direction of 'RectangleNorth'!::
		                rN
		
		       x2        ^         x4
		           ._____|_____.
		           |     |     |
		           |     |     |
		           |     |     |
		   rW  <---|-----x     | b
		           |           |
		           |           |
		           .___________.
		                 a
		       x1                  x3
		
			rW: rectangle West
			rN: rectangle North
			a:  SideA
			b:  SideB
		
	:param Origin: The center position of the rectangle (default: [0,0,0])
	:type Origin: float list
	:param Normal: The normal vector of the rectangle plane, photons are only considered by the observer, if the scalar product between their direction and the normal is pcStructuredAbsorptionPositionObserver
	:type Normal: float list
	:param RectangleNorth: A second vector to define the orientation of the rectangle (see drawing) (default: [1,0,0])
	:type RectangleNorth: float list
	:param SideA: The length of side A (see drawing) (default: No default, has to be specified!)
	:type SideA: float list
	:param SideB: The length of side B, in direction of RectangleNorth (see drawing) (default: No default, has to be specified!)
	:type SideB: float list
	:param Mode: can be either detector or sensor. In detector mode, the photon is killed after it has been detected. In sensor mode, it continues to live after it has been observed by the rectangle (default: 'sensor')
	:type Mode: {'sensor','detector'} 
	"""
	def NotImplemented(self):
		return False

	def __init__(self,*args,**kwargs):
		super(pcObserverRectangle,self).__init__(*args,**kwargs)	


	def _addParams(self):
		super(pcObserverRectangle,self)._addParams()
		self._addParam(FloatListParam('RectangleNorth',[1,0,0],min_length=3,max_length=3))
		self._addParam(FloatParam('SideA',-1))
		self._addParam(FloatParam('SideB',-1))



# ####################################################################################
# 
# VTK Related
# 
# ####################################################################################
def ErrorEventHandler(theObject, eventName, callData):
	"""
	due to the VTK Python wrappings, raising an exception here will
	only print the error, not go through the complete exceptiong handling
	trajectory - best we can do is to toggle some error flag somewhere
	and react on it later
	"""
	print "Oooops!  Error trapped (%s): %s" % (eventName,callData,)
	sys.exit(1)

ErrorEventHandler.CallDataType = "string0"
"""Set the CallDataType attribute to string0"""





# ####################################################################################
# 
# JSON Related
# 
# ####################################################################################
class PhotoncruncherEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, Configurable):
			return obj.to_json()
		if isinstance(obj, MonteCarloSimulator):
			return obj.to_json()


# class PhotoncruncherDecoder(json.JSONDecoder):
def PhotoncruncherDecoder(objstr):
	if '__type__' in objstr:
		try:
			obj = getattr(sys.modules[__name__],objstr['__type__'])(
				**objstr['kwargs']
				)
		except Exception as e:
			print 'error in json decoding near {}: \n\t{}'.format(objstr,e)
			sys.exit(1)

		return obj
		
	return objstr




def in_result_directory(fn):
	"""This decorator checks, if the simulator has already chdir'd into the result directory"""
	def deco(self):
		if self.in_result_directory == True:
			fn(self)
		else:
			print "ERROR: you have to be in the result directory to call the function %s" % (fn.__name__)
			sys.exit(1)

	return deco




def PopulateMaterialDatabase(matdbase):	

	matspecfile = resource_filename('pctools','data/materials.spec')
	matdbase += '.db'

	if not os.path.isfile(matdbase):
		raise ParameterSettingError('material database file not found: {}'.format(matdbase))

	mat = ConfigObj(matdbase,configspec=matspecfile,indent_type='\t')
	val = Validator()
	result = mat.validate(val,preserve_errors=False, copy=True)

	# print mat;
#		print result;
	if not result == True:
		print "Incorrect Material config: "
		for m in result:
			if not result[m] == True:
				print "Missing or incorrect value(s) in "+m+':'
				for n in result[m]:
					if not result[m][n] == True:
						print " -> "+n+" is missing";
						sys.exit(1)
	
	db = pc.pcMaterialDatabase_GetDB();
	pc.PLOG(pc.INFO,'\tpopulating material database')
	for m in mat:
		mt = pc.pcMaterial(mat[m]['ID'],\
							mat[m]['name'],\
							mat[m]['mu_a'],\
							mat[m]['mu_s'],\
							mat[m]['g'],\
							mat[m]['n'])
		pc.PLOG(pc.INFO,'\t - adding material: {}'.format(mt))
		if not db.AddMaterial(mt):
			print "Error adding material: " + mat[m]['name'] + \
				"already exists."
		
	return mat







class MonteCarloSimulator(object):
	"""
	MonteCarloSimulator is the main object to run a simulation. A few of the arguments are REQUIRED, others are optional. 

	:param mpih:			MPI Handler (pcMPIHandler object)
	:param debuglevel:		One of: ERROR, WARNING, INFO, DEBUG, DEBUG1, DEBUG2, DEBUG3, DEBUG4 (default: INFO)
	:param photonnumber:	Number of photons to be simulated (default: 1)
	:param consolelogging: 	Wheter or not logging output should be written to console additionally to file (default: True)
	:param ResultDir:		The directory to store simulation results (default: no default)
	:param rngtype:			The random number generator object (default: mt19937)
	:param geometry:		Geometry object (REQUIRED)
	:type geometry: pcGeometry
	:param source:			Source object (REQUIRED)
	:type source: pcPhotonSource
	:param method:			Monte-Carlo method (REQUIRED)
	:param materialDB:		path to material file
	"""

	def __init__(self,*args,**kwargs):

		self._kwargs = kwargs
		self._name = self.__class__.__name__

		#===========================================================#
		# First things first, start a new mpi handler if not provided
		#===========================================================#		
		pc.PLOG(pc.INFO,'Fetch mpi Handler...')
		# if not 'mpih' in self._kwargs:
		# 	pc.PLOG(pc.INFO,'\tno mpi handler found in argument list, creating a new one')
		# 	self.mpih = pcMPIHandler(len(sys.argv),sys.argv)
		# else:
		# 	self.mpih = self._kwargs.get('mpih')
		self.mpih = MPIH

		
		# set up the logger
		pc.PLOG(pc.INFO,'Setup logger...')
		pc.SetLogLevel(self._kwargs.get('debuglevel','INFO'))
	

		#===========================================================#
		# Create and initialize the main simulator
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup simulator...')
		self.simulator = pc.pcMonteCarloSimulator(self.mpih._obj)
	
		


		# since the config Object used doesn't support long, we read number of photons as string
		#    and convert this string to a long
		pc.PLOG(pc.INFO,'Setting photon number...')
		try:
			self.simulator.SetNumberOfPhotons(long(float(self._kwargs.get('photonnumber','1'))))
		except:
			raise ParameterSettingError('Something went wrong when trying to se the number of photons: I cant convert {} to a number!'.format(self._kwargs.get('photonnumber','1')))

		
		# do some run-time statistics
		pc.PLOG(pc.INFO,'Setup timer...')
		self.timer = pc.pcTimer()

		

		# save the current working path (before changing to the result directory)
		self.oldPath = os.getcwd();
		



		#===========================================================#
		# Create the Geometry Container
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup Geometry Container...')
		self.container = pc.pcGeometryContainer();
		self.simulator.SetGeometryContainer(self.container);

		self.in_result_directory = False

		self.timer.Start()
		pc.PLOG(pc.INFO,'Start Timer: ' + str(self.timer))


		self.callList = OrderedDict()

		self.callList['EnableConsoleLogging'] = any2bool(self._kwargs.get('consolelogging',True))
		self.callList['SetupResultDirectory'] = True
		self.callList['CreateResultDirectory'] = True
		self.callList['OpenLogFile'] = True
		self.callList['CreateMaterialDatabase'] = True
		self.callList['RandomNumberGenerator'] = True
		self.callList['Collector'] = True
		self.callList['GeometryObject'] = True
		self.callList['Source'] = True
		self.callList['Method'] = True
		self.callList['Observers'] = True
		self.callList['SaveSimulationEnvironment'] = True
		self.callList['RunSimulator'] = True
		self.callList['CleanUp'] = True
		

	def to_json(self):
		return { '__type__': self._name, 'kwargs': self._kwargs }



	def Exec(self):
		# print self.callList
		for step in self.callList.iteritems():
			if step[1]:
				try:
					
					pc.PLOG(pc.INFO,'')
					pc.PLOG(pc.INFO,'================================================================');
					pc.PLOG(pc.INFO,'=>\t {}'.format(step[0]))
					pc.PLOG(pc.INFO,'================================================================\n');
					getattr(self,step[0])()
				except Exception as e:
					pc.PLOG(pc.ERR,"Error during execution of step {0} in MonteCarloSimulator: {1.__class__!s} : {1!s}".format(step[0],e))
					# pc.PLOG(pc.ERR,u'Error during execution of step {} in MonteCarloSimulator\n'.format(step))
					sys.exit(1)
					


	def EnableConsoleLogging(self):
		if MPIH.GetRank() == 0:
			pc.EnableConsoleLogging()


	def SetupResultDirectory(self):
		#===========================================================#
		# Create the result directory, create logfile
		#===========================================================#
		
		res = self._kwargs.get('ResultDir','MCS__{}'.format(time.strftime("%a__%d-%m-%Y__%H-%M-%S", time.localtime())))
		
		if res[0] == '/':
			# An absolute path is provided
			self.resultDir = res
		else:
			self.resultDir = os.path.join(os.getcwd(),res)

		
	def CreateResultDirectory(self):
		ok = pc.CreateResultDirectory(self.resultDir,'o');
		if(ok > 0): # error occured during creation of result directory
			pc.PLOG(pc.ERR,"oops: error when trying to create the result directory " + self.resultDir)
			del self.mpih
			sys.exit(1)

		# change to the new directory
		os.chdir(self.resultDir);
		self.in_result_directory = True


	@in_result_directory
	def SaveSimulationEnvironment(self):
		# save the configuration to outfile
		# self.conf.filename = 'config'
		# if(self.mpih.GetRank() == pc.MPIMASTER):
		# 	self.conf.write();

		# save the material database used
		# pc.PLOG(pc.INFO,"Material Database: " + self.matdb.ToString());
		# self.matconf.filename = 'materials.db'
		# if(self.mpih.GetRank() == pc.MPIMASTER):
		# 	self.matconf.write();
		pass		


	@in_result_directory	
	def OpenLogFile(self):
		# create the log file

		lfn = self._kwargs.get('LogFileName','log')

		try: 
			pc.PLOG(pc.INFO,'opening log file... ' + lfn)
			pc.OpenLogFile(lfn);
		except Exception as e:
			pc.PLOG(pc.ERR,'Error when Opening Logfile:')
			pc.PLOG(pc.ERR,str(e))
			sys.exit(1)		


	def CreateMaterialDatabase(self):
		#===========================================================#
		# Create Material Database
		#===========================================================#
		# self.matdb = pc.mat.pcMaterialDatabase_GetDB();
		# self.matdb.Reset();
		pc.PLOG(pc.INFO,'Setup Material Database...')
		try:
			matdbpath = self._kwargs['materialDB']
		except:
			raise ParameterSettingError('materialDB is a required parameter')

		if not matdbpath[0] == '/':
			matdbpath = os.path.join(self.oldPath,matdbpath)

		if matdbpath[-3:] == '.db':
			# The actual file with ending .db was chosen
			matdbpath = matdbpath[:-3]
		self.matconf = PopulateMaterialDatabase(matdbpath);


	def RandomNumberGenerator(self):
		#===========================================================#
		# Create the Random Number Generator
		# Set the seed to the rank number of the mpi process
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup Random Number Generator')
		self.gslRNG = pc.pcGSLRandomNumberGenerator(self._kwargs.get('rngtype','mt19937'),self.mpih._obj.GetRank())
		self.simulator.SetRandomNumberGenerator(self.gslRNG)


	
	def Collector(self):
		#===========================================================#
		# Define a photon Collector
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup photon collector...')
		self.collector = pc.pcPhotonCollector();
		self.container.SetCollector(self.collector)
	



	@in_result_directory
	# @setter('container')
	def GeometryObject(self):
		#===========================================================#
		# Load geometry from file and create geometry object
		#===========================================================#
		
		# pc.PLOG(pc.INFO,'\n\n====================================================================')
		pc.PLOG(pc.INFO,'Setup Geometry Object...')
	
		try:
			self.geometry = self._kwargs['geometry']
		except:
			raise ParameterSettingError('No geometry object found in arguments')
		

		
		self.geometry.setup()



	# 	# check if the provided options are sufficient to correctly initialize the geometry object
		if not self.geometry._obj.IsCorrectlyInitialized():
			raise ParameterSettingError('The geometry was not initialized correctly.')


		# attach the geometry Object to the container
		self.container.SetGeometryObject(self.geometry._obj)
		
		if hasattr(self.geometry._obj,'GetVtkImageDataObject'):
			geo = self.geometry._obj.GetVtkImageDataObject()
			writer = vtk.vtkXMLImageDataWriter()
			writer.SetFileName('geometry.vti')
			writer.SetInput(geo)
			writer.Update()
	
	def Source(self):
		
		#===========================================================#
		# Define a photon source
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup Source Object ...')
		
		try:
			self.source = self._kwargs['source']
			self.source.setup()
		except:
			raise ParameterSettingError('No source object found in arguments')

		
		self.container.SetSource(self.source._obj)
	


	def Method(self):
		#===========================================================#
		# Define and setup a simulation method
		#===========================================================#
		# pc.PLOG(pc.INFO,'\n\n====================================================================')
		pc.PLOG(pc.INFO,'Setup Monte-Carlo method ...')
		try:
			self.method = self._kwargs['method']
			self.method.setup()
		except:
			raise ParameterSettingError('No method object found in arguments')


		
		
		pc.PLOG(pc.INFO,'     - setting pcMethod:GeometryObject')
		self.method._obj.SetGeometryObject(self.geometry._obj);
		
		
		self.simulator.SetMethod(self.method._obj)


	@in_result_directory
	def Observers(self):
		#===========================================================#
		# we want to observe some things:
		#===========================================================#
		pc.PLOG(pc.INFO,'Setup Observers ...')

		for observer in self._kwargs['observers']:
			observer.setup()
			self.simulator.AttachObserver(observer._obj)
			observer._obj.Init()

	

	@in_result_directory
	def RunSimulator(self):
		#===========================================================#
		# go!!
		#===========================================================#
		
		pc.PLOG(pc.INFO,'')
		pc.PLOG(pc.INFO,'================================================================');
		pc.PLOG(pc.INFO,'=>\t START SIMULATION')
		pc.PLOG(pc.INFO,'================================================================\n');
		
		try:
			self.simulator.run();
		except Exception as e:
			pc.PLOG(pc.ERR,"Runtime Error, no results will be printed!")
			pc.PLOG(pc.ERR,str(e))
			os.chdir(self.oldPath);
			return 0;
		



	def CleanUp(self):
		#===========================================================#
		# Clean-up 
		#===========================================================#
		
			
		pc.PLOG(pc.INFO,'Stop Timer. This simulation took ' + str(self.timer) + ' seconds.')
		self.timer.Stop()
		
		
		respath = os.getcwd()
		ressize = sum([os.path.getsize(f) for f in os.listdir(respath) if os.path.isfile(f)]);

		pc.PLOG(pc.INFO,'The simulation finished at ' + time.strftime("%a__%d-%m-%Y__%H-%M-%S", time.localtime()));
		pc.PLOG(pc.INFO,'The results are saved in ' + respath)
		pc.PLOG(pc.INFO,'The size of the result dir is ('+ str(ressize/1024/1024.0) +' MB)');

		# change to the old path
		os.chdir(self.oldPath);	




class Runner(object):
	__metaclass__ = ABCMeta


	def __init__(self,mcs=None,*args,**kwargs):


		if '--load-config' in sys.argv:
			f_ind = sys.argv.index('--load-config')
			sys.argv.pop(f_ind)
			filename = sys.argv.pop(f_ind)
			if not os.path.isfile(filename):
				print '\n\n\tI did not find the config file {}!\n\n'.format(filename)
				sys.exit(1)

			with open(filename,'r') as f:
				cnf = f.read()

			self.mcs = json.JSONDecoder(object_hook = PhotoncruncherDecoder).decode(cnf)

		else:
			self.mcs = mcs

		if '--help' in sys.argv or '-h' in sys.argv:

			f_ind = sys.argv.index('--help')
			try:
				if sys.argv[f_ind+1][0] != '-':
					# The argument successing --help is not starting with -, e.g. it's not a new command but an attribute to --help
					helpcmd = sys.argv[f_ind+1]
					if helpcmd == 'list':
						print 'MonteCarloSimulator'
						print '-------------------'
						print '\tMonteCarloSimulator (the main simulator object)'
						print '\nMethod'
						print '------'
						print '\tpcPhotonPacketVariableStepsizeStepping2'
						print '\nGeometry'
						print '--------'
						print '\tpcRegularGridGeometry'
						print '\tpcHomogeneousRegularGridGeometry'
						print '\tpcMaterial'
						print '\nSources'
						print '-------'
						print '\tpcIsotropicPhotonPointSource'
						print '\tpcPhotonPointSource'
						print '\tpcPhotonBeamSource'
						print '\nObservers'
						print '---------\n'
						print '\tpcStructuredFluenceObserver'
						print '\tpcObserverCircle'
						print '\tpcObserverRectangle'
						print '\tpcMaterialMarker'
					else:
						try:
							obj = getattr(sys.modules[__name__],helpcmd)
							print obj.__doc__
						except Exception as e:
							print 'No documentation available for {}'.format(helpcmd)
							sys.exit(1)

					
			except:
				print '\n   Photoncruncher   \n'
				print 'Commands:'
				print '\t --save-config filename: write the json-encoded config to file "filename"'
				print '\t --load-config filename: load a json-encoded config from "filename"'
				print '\t --print-config:         wrtie the json-encoded config to stdout'
				print '\t --run:                  run the simulator'
				# print '\t --gui:				  start with gui'

				print '\nClasses:'
				print '\tuse --help list to get a list of available objects'


		if '--save-config' in sys.argv:
			f_ind = sys.argv.index('--save-config')
			sys.argv.pop(f_ind)
			filename = sys.argv.pop(f_ind)
			# print filename
			if os.path.isfile(filename):
				ans = raw_input("\n\n\t{} exists. Overwrite? (y/[N])".format(filename))
				if not ans in ['y','Y']:
					print 'Aborting!'
					sys.exit(1)
				print '\n'
			cnf = json.dumps(self.mcs, cls = PhotoncruncherEncoder,indent=4)
			with open(filename,'w') as f:
				f.write(cnf)

			print '\tconfig written to {}\n\n'.format(filename)


		if '--print-config' in sys.argv:
			print json.dumps(self.mcs, cls = PhotoncruncherEncoder)


		if '--run' in sys.argv:
			self._run()


		if '--gui' in sys.argv:
			GUI = True

			try:
				import gui.photoncruncher_gui as gui
				print gui.Application
			except ImportError as e:
				# module doesn't exist, deal with it.
				GUI = False

			if GUI:
				print 'gui found'
			else:
				print 'gui not available. make sure qt4 is installed correctly'


	@abstractmethod	
	def _run(self):
		pass

class SingleRunner(Runner):


	def __init__(self,mcs=None,*args,**kwargs):
		super(SingleRunner,self).__init__(mcs,*args,**kwargs)
		

	def _run(self):
		if self.mcs:
			self.mcs.Exec()
		else:
			print 'Error: No MonteCarloSimulator object found. Abort'
			sys.exit(1)