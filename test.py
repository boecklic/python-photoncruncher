#!/usr/bin/env python
from pctools import pcIsotropicPhotonPointSource,MonteCarloSimulator,pcHomogeneousRegularGridGeometry,pcPhotonPacketVariableStepsize,SingleRunner,pcMaterial



source = pcIsotropicPhotonPointSource(Origin=[2,2,2])

mcs = MonteCarloSimulator(
		materialDB='materials.db',
		geometry=pcHomogeneousRegularGridGeometry(
			Origin=[1,1,1,],
			# Material=pcMaterial(Mu_a=10),
			),
		source=source,
		method = pcPhotonPacketVariableStepsize(),
		observers = [],
	)


SingleRunner(mcs)
# SingleRunner()

